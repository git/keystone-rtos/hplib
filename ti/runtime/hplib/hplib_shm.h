/**
 *   FILE NAME hplib_shm.h
 *
 *      (C) Copyright 2013 Texas Instruments, Inc.
 * 
 *  Redistribution and use in source and binary forms, with or without 
 *  modification, are permitted provided that the following conditions 
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the   
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

/**
 *   @file hplib_shm.h
 *
 *   @brief
 *      Header file for  High Performance Shared Memory. 
 *      The file exposes the data structures
 *      and exported API which are available for use by applications.
 *
*/
#ifndef __HPLIB_SHM_H__
#define __HPLIB_SHM_H__

#ifdef __cplusplus
extern "C" {
#endif
#include <stdint.h>
#include "src/hplib_loc.h"
#include "hplib_sync.h"



#define HPLIB_SHM_SIZE 0x100000 /*524288 bytes*/
#define HPLIB_SHM_ENTRIES 64


/**
 * HPLIB shared memory entry IDs.
 */
typedef enum {
    HPLIB_ENTRY     = 0,
    OSAL_ENTRY,
    NWAL_ENTRY,
    NETAPI_ENTRY,
    PKLIB_ENTRY,
    PA_ENTRY,
    SA_ENTRY,
    APP_ENTRY_1,
    APP_ENTRY_2,
    APP_ENTRY_3,
    APP_ENTRY_4,
    APP_ENTRY_5,
    APP_ENTRY_6,
    APP_ENTRY_7,
    APP_ENTRY_8,
    APP_ENTRY_9,
    APP_ENTRY_10,
    APP_ENTRY_11,
    APP_ENTRY_12,

    END_ENTRY       = HPLIB_SHM_ENTRIES-1,
} hplib_shmEntryId_E;

/** 
 * @brief 
 *  The structure is maintained by the HPLIB and contains information
 *  regarding the shared memory managed by HPLIB. 
 *
 * @details
 *  The structure is maintained by the HPLIB and contains information
 *  regarding the shared memory managed by HPLIB.
 *
 */
typedef struct hplib_shmInfo_Tag
{
    /**
    * @brief   Size of shared memory segment.
    */
    uint32_t size;

    /**
    * @brief   Offset to shared memory area which is un-consumed.
    */
    uint32_t free_offset;

    /**
    * @brief   Holds offset from base to different entries.
    */
    uint32_t toc[HPLIB_SHM_ENTRIES];

    /**
    * @brief   TBD.
    */
    int spinlock;
} hplib_shmInfo_T;


/**
 *  @b Description
 *  @n
 *      Create a shared memory segment
 *  @param[in] size
 *       size of shared memory segment to be created
 *  @retval
 *      void* base address of the shared memory segment, NULL on error
 */
void* hplib_shmCreate(int size);

/**
 *  @b Description
 *  @n
 *      Open a shared memory segment
 *  @retval
 *      void* base address of the shared memory segment, NULL on error
 */
void* hplib_shmOpen(void);

/**
 *  @b Description
 *  @n
 *      Delete a shared memory segment
 *  @retval
 *      hplib_RetValue, @ref hplib_RetValue
 */
hplib_RetValue hplib_shmDelete(void);

/**
 *  @b Description
 *  @n
 *      Add entry to shared memory segment
 *  @param[in] base
 *       base address of the shared memory segment
 *  @param[in] size
 *       size of entry being allocated from shared memory segment
 *  @param[in] entryId
 *       entryId of TBD

 *  @retval
 *      hplib_RetValue, @ref hplib_RetValue
 */
hplib_RetValue hplib_shmAddEntry(void* base, int size, hplib_shmEntryId_E entryId);



/**
 *  @b Description
 *  @n
 *      Get entry to shared memory segment
 *  @param[in] base
 *       base address of the shared memory segment
 *  @param[in] entryId
 *       Entry Id
 *  @retval
 *      void* address to TBD
 */
void* hplib_shmGetEntry(void* base, hplib_shmEntryId_E entryId);

#ifdef __cplusplus
}
#endif

#endif

