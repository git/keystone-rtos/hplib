/**
 *   FILE NAME hplib_vm.h
 *
 *      (C) Copyright 2012 Texas Instruments, Inc.
 * 
 *  Redistribution and use in source and binary forms, with or without 
 *  modification, are permitted provided that the following conditions 
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the   
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

/**
 *   @file hplib_vm.h
 *
 *   @brief
 *      Header file for the High Performance Virtual Memory Manager. 
 *      The file exposes the data structures
 *      and exported API which are available for use by applications.
 *
*/
#ifndef __HPLIB_VM_H__
#define __HPLIB_VM_H__

#ifdef __cplusplus
extern "C" {
#endif

#include <stdint.h>
#include "src/hplib_loc.h"
#include "hplib_cache.h"


/* Variables used by inline functions below */
/* virtual address of the [only] memory pool */
extern uint8_t *hplib_VM_mem_start;
extern uint8_t *hplib_VM_mem_end;
/* Physical address of the [only] memory pool */
extern uint8_t *hplib_VM_mem_start_phy;
extern uint8_t *hplib_VM_mem_end_phy;
extern uint32_t hplib_VM_virt_to_phy_mapping;
extern uint32_t hplib_VM_phy_to_virt_mapping;
extern hplib_virtualMemPoolAddr_T memPoolAddr[];

/** @defgroup hplib_memPoolAttr
 *  @ingroup HPLIB_VM_API
 */
 typedef int hplib_memPoolAttr;
/* @{ */
/**
 *  @def  HPLIB_ATTR_UN_CACHED
 *        Application provided physical address mory pool, un-cached.
 */
#define HPLIB_ATTR_UN_CACHED    0


/**
 *  @def  HPLIB_ATTR_KM_CACHED0
 *        Kernel module managed Cache enabled memory pool 0
 */
#define HPLIB_ATTR_KM_CACHED0    1

/// @cond FUTURE

/**
 *  @def  HPLIB_ATTR_KM_CACHED1
 *        Kernel module managed Cache enabled memory pool 1
 */
#define HPLIB_ATTR_KM_CACHED1    2

/// @endcond
/*  @}  */  /* ingroup */



/** 
 * @brief 
 *  The structure contains the HPLIB Virtual Memory Address Information for 
 *  QMSS/SRIO and PASS Perihperals. 
 *
 * @details
 * This structure is the 1st argument of the hplib_VM_MemoryInit API. 
 * is populated and returned to the user application when
 * hplib_VM_MemoryInit
 */
typedef struct hplib_virtualAddrInfo_Tag
{
    /**
    * @brief   Virtual memory address of QMSS configuration registers .
    */
    void *qmssCfgVaddr;

    /**
    * @brief   Virtual memory address of QMSS Data memory.
    */
    void *qmssDataVaddr;

    /**
    * @brief   Virtual memory address of SRIO configuration registers .
    */
    void *srioCfgVaddr;

    /**
    * @brief   Virtual memory address of PASS configuration registers.
    */
    void *passCfgVaddr;
} hplib_virtualAddrInfo_T;



/**
 * @brief
 *  The structure contains the HPLIB Physical Memory Address Device configuration for
 *  QMSS and PASS Perihperals.
 *
 * @details
 *  The structure contains the HPLIB Physical Memory Address Device configuration for
 *  QMSS and PASS Perihperals.
 */
typedef struct hplib_globalDeviceConfigParams_Tag
{
    uint32_t    cslNetCpCfgRegs;    /**< Base address of NETCP configuration Registers */
    uint32_t    netcpCfgBlkSz;      /**< Size of NETCP configuration Register block in bytes.*/
    uint32_t    cslQmssCfgBase;     /**< Base address of QMSS configuration Registers */
    uint32_t    qmssCfgBlkSz;       /**< Size of QMSS configuration Register block in bytes.*/
    uint32_t    qmssDataBlkSz;      /**< Size of QMSS Data Register block in bytes. */
} hplib_globalDeviceConfigParams_t;

/** 
 * @brief 
 *  The structure defines the attributes of a memory pool.
 *  */
typedef struct hplib_memPoolAttr_Tag
{
    /**
    * @brief   Attributes of memory pool.
    */
    hplib_memPoolAttr    attr;
    /**
    * @brief   Start physical address of memory pool
    */
    uint32_t    phys_addr;
    /**
    * @brief   Size of memory pool
    */
    uint32_t    size;
} hplib_memPoolAttr_T;

/**
@}
*/

/** @addtogroup HPLIB_VM_API
 @{ */


/**
 *  @b Description
 *  @n
 *     The function API is used to allocate continuous block of cached memory via CMA and optionally un-cached memoryif specified, 
 *     maps virtual memory for peripheral registers.
 *
 *  @param[in]  hplib_vmaddr
 *  @ref hplib_virtualAddrInfo_T
 *  @param[in]  num_pools
 *     number of memory pools being initialized
 *  @param[in] mempool_attr
 *      @ref HPLIB_MemPool_Attr_T
 *  @retval
 *      1 == Success, -1== Failure.
 */
int hplib_vmInit(hplib_virtualAddrInfo_T *hplib_vmaddr,
                 int num_pools,
                 hplib_memPoolAttr_T *mempool_attr);
/**
 *  @b Description
 *  @n
 *      The function API is used to release/unmap continuous block of memory allocated via 
 *      hplib_VM_MemorySetup function, remove mapping of virtual memory for peripherals.
 */
void hplib_vmTeardown(void);


/**
 *  @b Description
 *  @n  
 *      The function API is used to allocate memory from the specified pool id
 * 
 *  @param[in] size
 *      Size of block to allocate in bytes
 *  @param[in] align
 *      alignment of the block needed in bytes
 *  @param[in] pool_id
 *      pool id from which memory allocation request is being made
 *  @retval
 *      virtual memory pointer to allocated memory buffer.
 */
void* hplib_vmMemAlloc
(
    uint32_t    size,
    uint32_t    align,
    int         pool_id
);

/**
 *  @b Description
 *  @n
 *      The function API is used to free memory from the specified pool id
 *  @param[in] ptr
 *      ptr to memory being freed
 *  @param[in] size
 *      Size of memory being freed in bytes
 *  @param[in] pool_id
 *      pool id from which memory free request is being made
 *  @retval
 *      none
 */
void hplib_vmMemFree
(
    void        *ptr,
    uint32_t    size,
    int         pool_id
);


/**
 *  @b Description
 *  @n
 *  The function API returns the free amount of buffer/descriptor area for the memory pool specified.
 
 *  @param[in] pool_id
 *      pool id for which free amount of buffer/descriptor area is being returned.
 *  @retval
        *Amount of free amount of buffer/descriptor area in bytes
 */
uint32_t hplib_vmGetMemPoolRemainder(int pool_id);

/**
 *  @b Description
 *  @n
 *      The function API is used to convert a physical address to a virtual address for physical address
 * 
 *  @param[in] ptr
 *      Physical address pointer
 *  @retval
 *      Virtual address pointer
 */
static inline void * hplib_mVMPhyToVirt (void *ptr)
{
    if (!ptr)
        return (void *) hplib_OK;
    if (((uint8_t *)ptr <hplib_VM_mem_start_phy)||( (uint8_t *)ptr>hplib_VM_mem_end_phy))
        return (void *) hplib_OK;

    return (void *) (hplib_VM_phy_to_virt_mapping +(uint8_t*)ptr);
}

/**
 *  @b Description
 *  @n
 *  The function API is used to convert a virtual address to a physical address for virtual address
 * 
 *  @param[in] ptr
 *      virtual address pointer
 *  @retval
        *Physical address pointer
 */
static inline void* hplib_mVMVirtToPhy (void *ptr)
{
    if (!ptr) return (void *) 0;
    if (((uint8_t *)ptr <hplib_VM_mem_start)||( (uint8_t *)ptr>hplib_VM_mem_end))
        return (void *) hplib_OK;

    return (void *) (hplib_VM_virt_to_phy_mapping +(uint8_t*)ptr);
}


/*
 *  @b Description
 *  @n
 *      The function API is used to convert a physical address to a virtual address for physical address
 *      for specified memory pool ID
 *
 *  @param[in] ptr
 *      Phsycial address pointer
 *  @param[in] pool_id
 *      Memory pool ID
 *  @retval
 *      Virtual address pointer
 */
static inline void * hplib_mVMPhyToVirtPoolId (void *ptr, uint8_t pool_id)
{
    if (pool_id > (HPLIB_MAX_MEM_POOLS -1))
                return (void *) hplib_OK;
    if (!ptr) return
        (void *) hplib_OK;
    if (((uint8_t *)ptr <memPoolAddr[pool_id].memStartPhy)||( (uint8_t *)ptr>memPoolAddr[pool_id].memEndPhy))
        return (void *) hplib_OK;

    return (void *)(memPoolAddr[pool_id].memStart + ((uint8_t*)ptr - memPoolAddr[pool_id].memStartPhy));
}

/*
 *  @b Description
 *  @n
 *  The function API is used to convert a virtual address to a physical address for virtual address
  *      for specified memory pool ID
 *
 *  @param[in] ptr
 *      Phsycial address pointer
 *  @param[in] pool_id
 *      Memory pool ID
 *  @retval
 *      Physical address pointer
 */
static inline void* hplib_mVMVirtToPhyPoolId(void *ptr, uint8_t pool_id)
{

    if (pool_id > (HPLIB_MAX_MEM_POOLS -1))
                return (void *) hplib_OK;
    if (!ptr) return (void *) hplib_OK;
    if (((uint8_t *)ptr <memPoolAddr[pool_id].memStart)||( (uint8_t *)ptr>memPoolAddr[pool_id].memEnd))
        return (void *) hplib_OK;

    return (void *)(memPoolAddr[pool_id].memStartPhy+ ((uint8_t*)ptr - memPoolAddr[pool_id].memStart));
}

/**
 *  @b Description
 *  @n  
 *      The function API is used to traverse a CPPI host/monolithic descriptor and perform
 *      a virtual address to physical address translation on all address references in the descriptor
 *  @param[in] descAddr
 *      Virtual address of descriptor
 *
 *  @retval
 *      none
 */
static inline void* hplib_mVMConvertDescVirtToPhy(void *descAddr)
{
    if (!descAddr) return (void *)hplib_OK;
#ifdef ASSUME_ALL_DESCRIPTOR 
    if (Cppi_getDescType((Cppi_Desc *)QMSS_DESC_PTR(descAddr)) == Cppi_DescType_HOST)
#endif
    {
        Cppi_HostDesc *nextBDPtr = (Cppi_HostDesc *)QMSS_DESC_PTR(descAddr);
        Cppi_HostDesc *prevBDPtr = 0;
        while (nextBDPtr)
        {
            void *buffPtr=NULL;
            if (nextBDPtr->buffPtr)
            {
                buffPtr = (void *)nextBDPtr->buffPtr;
                nextBDPtr->buffPtr = (uint32_t)hplib_mVMVirtToPhy((void *)(nextBDPtr->buffPtr));
                if (!(nextBDPtr->buffPtr)) return (void *) hplib_OK;
                hplib_cacheWb(buffPtr, nextBDPtr->buffLen);          
            }

            if (nextBDPtr->origBuffPtr)
            {
                nextBDPtr->origBuffPtr = (uint32_t)hplib_mVMVirtToPhy((void *)(nextBDPtr->origBuffPtr));
                if (!(nextBDPtr->origBuffPtr)) return (void *) hplib_OK;
            }

            prevBDPtr = nextBDPtr;
            nextBDPtr = (Cppi_HostDesc *)QMSS_DESC_PTR((nextBDPtr->nextBDPtr));
            if (prevBDPtr->nextBDPtr)
            {
                prevBDPtr->nextBDPtr = (uint32_t)hplib_mVMVirtToPhy((void *)(prevBDPtr->nextBDPtr));
                if (!(prevBDPtr->nextBDPtr)) return (void *) hplib_OK;
            }

            hplib_cacheWb(prevBDPtr, TUNE_HPLIB_DESC_SIZE);
        }
        descAddr = hplib_mVMVirtToPhy(descAddr);
        if (!descAddr) return (void *) hplib_OK;
    }
#ifdef ASSUME_ALL_DESCRIPTOR 
    else if (Cppi_getDescType((Cppi_Desc *)QMSS_DESC_PTR(descAddr)) == Cppi_DescType_MONOLITHIC)
    {
        hplib_cacheWb(descAddr, TUNE_HPLIB_DESC_SIZE);
        descAddr = hplib_mVM_VirtToPhy(descAddr);
        if (!descAddr) return (void *) hplib_OK;
    }
#endif
    /* Issue memory barrier */
    hplib_mMemBarrier();
    return descAddr;

}

/**
 *  @b Description
 *  @n  
 *      The function API is used to traverse a CPPI host/monolithic descriptor and perform
 *      a physical address to virtual address translation on all address references in the descriptor
 *  @param[in] descAddr
 *      Physical  address of descriptor
 *   @retval
 *      none
 */
static inline void* hplib_mVMConvertDescPhyToVirt(void *descAddr)
{
    if (!descAddr) return (void *)0;
    descAddr = hplib_mVMPhyToVirt(descAddr);

#ifdef ASSUME_ALL_DESCRIPTOR
    if (Cppi_getDescType((Cppi_Desc *)QMSS_DESC_PTR(descAddr)) == Cppi_DescType_HOST)
#endif
    {
        Cppi_HostDesc *nextBDPtr = (Cppi_HostDesc *)QMSS_DESC_PTR(descAddr);
        while (nextBDPtr)
        {
            hplib_cacheInv(nextBDPtr, TUNE_HPLIB_DESC_SIZE);
            if (nextBDPtr->buffPtr)
            {
                nextBDPtr->buffPtr = (uint32_t)hplib_mVMPhyToVirt((void *)(nextBDPtr->buffPtr));
                if (!(nextBDPtr->buffPtr)) return (void *) hplib_OK;
                hplib_cacheInv((void *)(nextBDPtr->buffPtr), nextBDPtr->buffLen);
            }

            if (nextBDPtr->origBuffPtr)
            {
                nextBDPtr->origBuffPtr = (uint32_t)hplib_mVMPhyToVirt((void *)(nextBDPtr->origBuffPtr));
                if (!(nextBDPtr->origBuffPtr)) return (void *) hplib_OK;
            }

            if (nextBDPtr->nextBDPtr)
            {
                nextBDPtr->nextBDPtr = (uint32_t)hplib_mVMPhyToVirt((void *)(nextBDPtr->nextBDPtr));
                if (!(nextBDPtr->nextBDPtr)) return (void *) hplib_OK;
            }

            nextBDPtr = (Cppi_HostDesc *)QMSS_DESC_PTR((nextBDPtr->nextBDPtr));
        }
    }
#ifdef ASSUME_ALL_DESCRIPTOR
    else if (Cppi_getDescType((Cppi_Desc *)QMSS_DESC_PTR(descAddr)) == Cppi_DescType_MONOLITHIC)
    {
        descAddr = hplib_mVMPhyToVirt(descAddr);
        if (!descAddr) return (void *) hplib_OK;
        hplib_cacheInv(descAddr, TUNE_HPLIB_DESC_SIZE);
    }
#endif
    return descAddr;
}

/**
 *  @b Description
 *  @n
 *      The function API is used to reset the MALLOC area for a memory pool
 *  @param[in] pool_id
 *      memory pool_id
 *  @retval
 *      hplib_RetValue, @ref hplib_RetValue
 */
hplib_RetValue hplib_resetMallocArea(int pool_id);

/**
 *  @b Description
 *  @n
 *      The function API is used to initialize the MALLOC area for a memory pool
 *  @param[in] pool_id
 *      memory pool_id
 *  @retval
 *      hplib_RetValue, @ref hplib_RetValue
 */
hplib_RetValue hplib_initMallocArea(int pool_id);

/**
 *  @b Description
 *  @n
 *      The function API is used to check the MALLOC area for a memory pool
 *      to make sure it has been initialized.
 *  @param[in] pool_id
 *      memory pool_id
  *  @retval
 *      hplib_RetValue, @ref hplib_RetValue
 */
hplib_RetValue hplib_checkMallocArea(int pool_id);

/**
 *  @b Description
 *  @n
 *      The function API is used to map the given physical address
 *      to virtual memory space.
 *  @param[in] addr
 *          Physical address pointer of memory space
 *  @param[in] size  size of memory to be mapped
 *  @retval
 *      Virtual address pointer of mapped memory space
 */
void *hplib_VM_MemMap(void* addr, uint32_t size);

#ifdef __cplusplus
}
#endif

#endif

