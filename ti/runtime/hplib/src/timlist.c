/******************************************
 * FILE:timlist.c 
 * Purpose:  timer low level primitives
 ***********************************************
* FILE:timlist.c
 * 
 * DESCRIPTION:  hplib timer library source file for user space transport
 *               library
 * 
 * REVISION HISTORY:  rev 0.0.1 
 *
 *  Copyright (c) Texas Instruments Incorporated 2010-2011
 * 
 *  Redistribution and use in source and binary forms, with or without 
 *  modification, are permitted provided that the following conditions 
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the   
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 ***************************************/
//#define TEST_DRIVER

#ifdef CORTEX_A8
#ifdef TEST_DRIVER
//typedef void * NETAPI_T;
typedef void * HPLIB_TIMER_CB_T;
#else
//#include "hplib_timer.h"
#endif
#include "timer_loc.h"
#include "hplib_util.h"


#ifndef NULL
#define NULL 0
#endif


//create a timer group
int tim_group_create(TIMER_GROUP_T *g, int n_cells, int n_timers)
{
char * mem= (char *) malloc(n_timers * sizeof (TIM_T));
if (!mem) return -1;
g->n_cells = n_cells;
tim_build_free(&g->free, mem, sizeof(TIM_T) * n_timers);
g->cells=(TIM_LIST_T *) malloc(n_cells * sizeof(TIM_LIST_T));
if (!g->cells) {free(mem);  return -1;}
g->n_timers=n_timers;
g->last_polled=0;
return 1;
}

//return as single timer to end of free list, zero cookie, etc
void tim_return_single_free(TIM_LIST_T *ftl,  TIM_T *p)
{
TIM_T * pt;
int i;
pt = ftl->tail;
if (!pt)
{
ftl->head=p;
ftl->tail=p;
}
else
{
pt->next=p;
ftl->tail = p;
}
  p->t=0LL;
  p->cookie=NULL;
  p->next=NULL;
}


//return a list of N timers to  end of [free] list.  zero cookie, etc 
void tim_return_free(TIM_LIST_T *ftl,  TIM_LIST_T *p, int n)
{
TIM_T * pt;
int i;
pt = ftl->tail;
if (!pt) 
{
ftl->head=p->head;
ftl->tail=p->tail;
}
else 
{
pt->next=p->head;
ftl->tail = p->tail;
}

pt= p->head;
for(i=0;i<n;i++)
{
  pt->t=0LL;
  pt->cookie=NULL;
  pt= pt->next;
}
ftl->tail->next=NULL;
}

//get a [free] entry from front of a list 
TIM_T * tim_get_free(TIM_LIST_T *tl, void *cookie, unsigned long long t)
{
TIM_T *p = tl->head;
if  (p)
{
   tl->head=p->next;
   p->next=NULL;
   p->cookie = cookie;
   p->t = t;
   if (!tl->head) tl->tail=NULL;
}
return p;
}

//build a timer list from chunk of memory
int tim_build_free(TIM_LIST_T *tl, char *p_mem, int mem_size)
{
TIM_T * p = (TIM_T*) p_mem;
TIM_T * p_old = (TIM_T*) p_mem;
int i=0;
tl->head = p;
while(mem_size)
{
   p_old = p;
   p->cookie=NULL;
   p->t = 0LL;
   mem_size -= sizeof(TIM_T);
   p+=1;
   i+=1;
   p_old->next=p;
}
p_old->next = NULL;
tl->tail = p_old;
return i;
}

//return a list of timers that have fired
void tim_return_fired_list(TIM_LIST_T *tl, TIM_LIST_T * pr, TIM_LIST_T * ftl, unsigned long long t,  int * p_nf)
{
int i=0;
int got_start=0;
int found_end=0;
TIM_T * p_last, *p, *temp_p_next;
TIM_LIST_T p_free={NULL,NULL};
if (! tl->head) { *p_nf=0; return ;}

p =  tl->head;
p_last= NULL; 

for(i=0;p;)
{
  if(p->t <= t) 
  {
    if(!got_start)
    {
       if(p->cookie)
       {
       //start results chain
         got_start=1;
         if(pr->tail)  {pr->tail->next = p; pr->tail=p;}
         else {pr->head=pr->tail = p;}
         i+=1;
         p_last=p;
         p=p->next;
         continue;
       }
       else //skip over cancelled timer..
       {
         //skip & free. make sure we adjust head or tail if necessary
         if (p_last){
             p_last->next=p->next; 
         }
         else {
            tl->head = p->next; /* we are freeing old head..*/
         }
         if (tl->tail ==p)  tl->tail=p_last; /* we are freeing old tail */
         temp_p_next=p->next;
         tim_return_single_free(ftl,p);
         p=temp_p_next; 
         /*keep p_last the same! */
         continue;     
       }
    } 
    else
    {
      if(!p->cookie)
      {
         //skip & free
         if (p_last){ 
             p_last->next=p->next; 
         }
         else {
            tl->head = p->next;
         }
         if (tl->tail ==p)  tl->tail=p_last; /* we are freeing old tail */
         temp_p_next=p->next;
         tim_return_single_free(ftl,p);
         p=temp_p_next;      
         /*keep p_last the same! */
         continue;
      }  
      else { //valid entry for list.
        p_last=p; 
        p=p->next;
        i+=1;
        continue;
      }  
    }
  }
  else  /* p->t > t */
  {
    if(got_start)
    {
       found_end =1; //found end of chain to return.  All is good
       pr->tail = p_last;
       p_last->next=NULL;
       tl->head=p;
    }
    // done
    break;
  }
}

*p_nf=i;
if ((got_start) && (!found_end)) 
{
  //cleared the list
  //DEBUGprintf("clearingthelist\n");
  tl->head = tl->tail=NULL;
}
return;
}

//cancel a timer
void tim_cancel(TIM_T * p, int *pErr)
{
 *pErr =0;
 if (!p) {*pErr = hplib_ERR_BAD_INPUT; return; }
 if (!p->cookie) {*pErr= hplib_ERR_ALREADY_CANCELLED; return;}
 p->cookie = NULL;
 return;
}

//set a timer 
TIM_T *tim_set(TIM_LIST_T *tl, TIM_LIST_T *free_tl, unsigned long long t, void *cookie, int *pErr)
{
TIM_T *pt = tl->head;
TIM_T *p = tim_get_free(free_tl, cookie, t);
TIM_T *pt_last= NULL;
int i;
*pErr=0;
if (!p ) { *pErr=hplib_ERR_NOMEM; return NULL;}  

if (!pt) //first one
{
  tl->head = p;
  tl->tail =p;
  return p;  
}

//see if we can place at front of list
if (pt->t >=t)
{
  tl->head=p;
  p->next=pt;
  return p;
}

 //timer has hashed into this chain.  find out where
for(;pt;)
{
   if (pt->t >= t)       
   {
      if (pt_last) {
        pt_last->next = p;
        p->next=pt;
        return p;
      }
      else
      {
        tl->head=p;
        p->next=pt;
        return p;
      }
   }
   else {pt_last=pt;pt=pt->next;}
}
//last one
pt_last->next=p;
p->next=NULL;
tl->tail=p;
return p;
}

#ifdef TEST_DRIVER
TIM_LIST_T the_base={NULL,NULL};
TIM_LIST_T cell_base={NULL,NULL};
char *mem;
TIM_T * timers[10];
TIM_LIST_T res;
main()
{
 int err;
 mem= malloc(100*sizeof(TIM_T));
 TIM_T *p;
 int n;
 int i;
 
 tim_build_free(&the_base, mem , 100*sizeof(TIM_T));

 timers[0]=tim_set(&cell_base, &the_base, 100LL, (void *) 1, &err);
 timers[1]=tim_set(&cell_base, &the_base, 101LL, (void *) 2, &err);
 timers[2]=tim_set(&cell_base, &the_base, 105LL, (void *) 3, &err);
 timers[3]=tim_set(&cell_base, &the_base,  95LL, (void *) 4, &err);
 timers[4]=tim_set(&cell_base, &the_base,  95LL, (void *) 5, &err);
 timers[5]=tim_set(&cell_base, &the_base,  95LL, (void *) 6, &err);
 timers[6]=tim_set(&cell_base, &the_base,  104LL, (void *) 7, &err);
 timers[7]=tim_set(&cell_base, &the_base,  104LL, (void *) 8, &err);
 timers[8]=tim_set(&cell_base, &the_base,  104LL, (void *) 9, &err);

 tim_cancel(timers[6], &err);


 res.head=res.tail=NULL;
for(i=90;i<106;i++)
 {
tim_return_fired_list(&cell_base, &res, &the_base, (unsigned long long) i,  &n);
 printf("at %d got %d\n", i, n);
 if (n) {tim_return_free(&the_base,&res,n); res.head=res.tail=NULL;

 }

//special cases..
 res.head=res.tail=NULL;
 timers[0]=tim_set(&cell_base, &the_base, 106LL, (void *)10, &err);
 tim_cancel(timers[0],&err);
 tim_return_fired_list(&cell_base, &res, &the_base, (unsigned long long) 106,  &n);
 
 res.head=res.tail=NULL;
 timers[0]=tim_set(&cell_base, &the_base, 106LL, (void *)10, &err);
 timers[1]=tim_set(&cell_base, &the_base, 106LL, (void *)10, &err);
 tim_cancel(timers[0],&err);
 tim_return_fired_list(&cell_base, &res, &the_base, (unsigned long long) 106,  &n);

 res.head=res.tail=NULL;
 timers[0]=tim_set(&cell_base, &the_base, 106LL, (void *)10, &err);
 timers[1]=tim_set(&cell_base, &the_base, 106LL, (void *)10, &err);
 tim_cancel(timers[0],&err);
 tim_cancel(timers[1],&err);
 tim_return_fired_list(&cell_base, &res, &the_base, (unsigned long long) 106,  &n);
 
}
#endif

#endif
