/******************************************************************************
 * FILE PURPOSE:  Main Routines for HPLIB Utiliy APIs
 ******************************************************************************
 * FILE NAME:   hplib_vm.c
 *
 * The HPLIB provides a set of utility APIs which provide the following functionality
 *      API which returns a 64bit H/W timestamp by reading a H/W timer (TIMER64 on TCI6614 SOC, 
 *      A15 timer on TCI6638) and returns a 64 bit time stamp. 
 *      API to traverse a CPPI host/monolithic descriptor and perform  a virtual address to physical 
 *      address translation on all address references in the descriptor.
 *      API utility to traverse a CPPI host/monolithic descriptor and perform a physical address to virtual address translation 
 *      on all address references in the descriptor.
 *      API to read the current ARM PMU CCNT register( this counts CPU cycles).
 *      Access to ARM PMU event counters to be used for profiling purpose.
 
 *
 * REVISION HISTORY:
 *
 *  Copyright (c) Texas Instruments Incorporated 2010-2012
 * 
 *  Redistribution and use in source and binary forms, with or without 
 *  modification, are permitted provided that the following conditions 
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the   
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <errno.h>
#include <unistd.h>
#include <sys/ioctl.h>
#include "hplibmod.h"
#include "hplib.h"
#include "hplib_loc.h"
#include <pthread.h>
#include <sched.h>
#include <linux/unistd.h>
#include <sys/syscall.h>
#include <sys/sysinfo.h>
#include <sys/types.h>
#include <sys/stat.h>

extern void Osal_nwalSetProcId (uint16_t core_id );
void Osal_setHplibSpinLockIfType(hplib_spinLock_Type if_type);

/* file descriptor handle for hplib kernel device module */
int hplib_mod_fd = -1;

/**************************************************************************
 *  FUNCTION PURPOSE:   Opens the hplib kernel module
 **************************************************************************/
int hplib_utilModOpen(void)
{
    if (hplib_mod_fd != -1)
        return hplib_mod_fd;

    hplib_mod_fd = open("/dev/hplib", O_RDWR);

    if (hplib_mod_fd == -1)
    {
        hplib_Log("hplib_utilModOpen error\n");
        return -1;
    }
    return hplib_mod_fd;
}

/**************************************************************************
 *  FUNCTION PURPOSE:   Closes the hplib kernel module
 **************************************************************************/
void hplib_utilModClose(void)
{
    close(hplib_mod_fd);
    hplib_mod_fd = -1;
}

/**************************************************************************
 *  FUNCTION PURPOSE:   Return the size of memory region allocated via CMA
 **************************************************************************/
unsigned long hplib_utilGetSizeOfBufferArea(void)
{
    unsigned long size;

    if (ioctl(hplib_mod_fd, HPLIBMOD_IOCGETSIZE | HPLIBMOD_IOCMAGIC, &size) == -1)
    {
        return 0;
    }
    return size;
}

/******************************************************************************
 *  FUNCTION PURPOSE:   Return the physical adrress of memory region allocated via CMA
 ******************************************************************************/
unsigned long hplib_utilGetPhysOfBufferArea(void)
{
    unsigned long physp;

    if (ioctl(hplib_mod_fd, HPLIBMOD_IOCGETPHYS | HPLIBMOD_IOCMAGIC, &physp) == -1)
    {
        return 0;
    }
    return physp;
}

/******************************************************************************
 *  FUNCTION PURPOSE:   mmap the block into our user space process memory map
 ******************************************************************************/
unsigned long hplib_utilGetVaOfBufferArea(unsigned int offset, unsigned int size)
{
    void *userp;

    /* Map the physical address to user space */
    userp = mmap(0,                       // Preferred start address
                        size,            // Length to be mapped
                        PROT_WRITE | PROT_READ,  // Read and write access
                        MAP_SHARED,              // Shared memory
                        hplib_mod_fd,               // File descriptor
                        offset);                      // The byte offset from fd

    if (userp == MAP_FAILED)
    {
        hplib_Log("hplib_utilGetVaOfBufferArea: hplib kern module mmap failed\n");
        return 0;
    }
    return (unsigned long)userp;
}

pid_t gettid( void )
{
    return syscall( __NR_gettid );
}


hplib_RetValue hplib_utilInitProc()
{
    if (ioctl(hplib_mod_fd, HPLIBMOD_IOINIT | HPLIBMOD_IOCMAGIC, NULL) == -1)
    {
        hplib_Log("hplib_utilInitProc: HPLIBMOD_IOINIT IOCTL error\n");
        return 0;
    }
    return hplib_OK;

}

hplib_RetValue hplib_utilSetupThread(int threadId,
                                     cpu_set_t *pSet,
                                     hplib_spinLock_Type type)
{
    if (hplib_mod_fd == -1)
    {
        hplib_utilModOpen();
    }
    if (pSet && sched_setaffinity( gettid(), sizeof( cpu_set_t ), pSet ))
    {
        return hplib_FAILURE;
    }
    Osal_nwalSetProcId(threadId);
    Osal_setHplibSpinLockIfType(type);
    return (hplib_utilInitProc());
}




