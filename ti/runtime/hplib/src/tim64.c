/*
 * tim64.c  : enable use of timer64 from user space
 *  (using 64 bit mode)
 *   TIMER 6
 **************************************************************
 * FILE: tim64.c  
 * 
 * DESCRIPTION:  tim64 peripheral driver for user space transport
 *               library
 * 
 * REVISION HISTORY:  rev 0.0.1 
 *
 *  Copyright (c) Texas Instruments Incorporated 2010-2011
 * 
 *  Redistribution and use in source and binary forms, with or without 
 *  modification, are permitted provided that the following conditions 
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the   
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>
#include <signal.h>
#include <fcntl.h>
#include <ctype.h>
#include <termios.h>
#include <sys/types.h>
#include <sys/mman.h>
#include "hplib_util.h"
#include "hplib_loc.h"

#ifdef CORTEX_A8
/* timing */
static inline unsigned long timing_start(void)
{
    volatile int vval;
    //read clock
    asm volatile("mrc p15, 0, %0, c9, c13, 0" :  "=r"(vval));
    return vval;
}

static inline unsigned long timing_stop(void)
{
    volatile int vval2;
    //read clock
    asm volatile("mrc p15, 0, %0, c9, c13, 0" :  "=r"(vval2));
    return vval2;
}


  
#define MAP_SIZE 4096UL
#define MAP_MASK (MAP_SIZE - 1)
//this is for timer 6
#define T64BASE_6  (void *)0x02260000
#ifdef CORTEX_A8
volatile unsigned long * t64_virt_addr;
#endif
static unsigned long tv_lo;
static unsigned long tv_hi;

//read
static unsigned long long read_t64(void)
{
    volatile unsigned long long t1;
    volatile unsigned long long t2;
    unsigned long long val;
    t1 = t64_virt_addr[0x10/4]; //lo
    t2 = t64_virt_addr[0x14/4]; //hi

    val = ((unsigned long long) t2) <<32 | t1;
    return val;

}

//claibrate
static unsigned int t64_cpu_cycle_per_tick=0;
unsigned int cpu_cycles_sec=983040000;


unsigned long t64_ticks_sec(void)
{
    if (t64_cpu_cycle_per_tick)
        return ( cpu_cycles_sec/t64_cpu_cycle_per_tick); //ticks per/sec
    else return 166666666;

}
static int t64_calibrate(int n)
{
    volatile unsigned long t1;
    volatile unsigned long t2;
    volatile unsigned long long t164;
    volatile unsigned long long t264;
    int i;
    volatile int s;
    t1=timing_start();
    t164=read_t64();

    sleep(1);
#if 0
    for(i=0;i<n;i++)
    {
     s+=t164*20; s=s*(2+i);
    }
#endif
    t264=read_t64();
    t2=timing_stop();
    t64_cpu_cycle_per_tick = (unsigned long) ((t2-t1)/(t264-t164));
    hplib_Log("netapi:  tim64 calibration - n=%d t2-t1=%lu t264-t164=%llu ccpt=%ld tps=%ld\n",
           n, t2-t1, t264-t164, t64_cpu_cycle_per_tick,  t64_ticks_sec());

    t1=timing_stop();
    t164=hplib_mUtilGetTimestamp();
    t264=hplib_mUtilGetTimestamp();
    t2=timing_stop();
    hplib_Log("netapi:  tim64 cycle cost= %d(cpu ticks),  back2back= %lld\n", (t2-t1)/2, t264-t164);

}


/*********************************
 * memory map t64 into user space
t264=read_t64();
t264=read_t64();
 *   input: pass in fd for /dev/mem
 **********************************/
int t64_memmap(int fd)
{
     off_t t64_base= (off_t) T64BASE_6;
     void * map_base;
     int op;
     volatile unsigned long t1;
     volatile unsigned long t2;
     unsigned long long blah;


    /* Map one page */
    map_base = mmap(0, MAP_SIZE, PROT_READ | PROT_WRITE, MAP_SHARED, fd, t64_base & ~MAP_MASK);
    if(map_base == (void *) -1) exit(-99);
    fflush(stdout);

    t64_virt_addr = (long *) map_base + (t64_base & MAP_MASK);
    hplib_Log("netapi timer64:  T64 Memory mapped at address %p.\n", t64_virt_addr);
    fflush(stdout);
    return 1;
}


 /*********************************
 *  start the timer64
 ***********************************/
int t64_start(void)
{
      t64_virt_addr[0x24/4]= 0x00;
      t64_virt_addr[0x10/4]= 0x00;
      t64_virt_addr[0x14/4]= 0x00;
      t64_virt_addr[0x18/4]= 0xffffffff;
      t64_virt_addr[0x1c/4]= 0xffffffff;
      t64_virt_addr[0x20/4]= 0x80;
      t64_virt_addr[0x24/4]= 0x03; //go
      t64_calibrate(100000);
      return 1;
}


#ifdef TEST_DRIVER
int main(int argc, char **argv) {
    int fd;
     off_t t64_base= (off_t) T64BASE_6; 
    void * map_base;
    int op;
    volatile unsigned long t1;
    volatile unsigned long t2;
    unsigned long long blah;

    if(argc < 2)
    {
        fprintf(stderr, "Usage: tim64 start|stop|read  \n");
        exit(1);
    }
    if (!strcmp(argv[1],"start"))
        op=0;
    else if (!strcmp(argv[1],"stop"))
        op =1;
    else if (!strcmp(argv[1],"read"))
        op =2;
    else
    {
        fprintf(stderr, "Usage: tim64 start|stop|read  \n");
        exit(1);
    }


    if((fd = open("/dev/mem", O_RDWR | O_SYNC)) == -1)
        exit(-99);
    hplib_Log("/dev/mem opened.\n"); 
    fflush(stdout);
    
    /* Map one page */
    map_base = mmap(0, MAP_SIZE, PROT_READ | PROT_WRITE, MAP_SHARED, fd, t64_base & ~MAP_MASK);
    if(map_base == (void *) -1)
        exit(-99);
    printf("mapbase=Memory mapped at address %p.\n", map_base); 
    fflush(stdout);
    
    t64_virt_addr = (long *) map_base + (t64_base & MAP_MASK);
    printf("t64_virt_ddr: Memory mapped at address %p.\n", t64_virt_addr); 
    fflush(stdout);

    switch(op)
    {
        case(0):
        default:
            //start
            t64_virt_addr[0x24/4]= 0x00;
            t64_virt_addr[0x10/4]= 0x00;
            t64_virt_addr[0x14/4]= 0x00;
            t64_virt_addr[0x18/4]= 0xffffffff;
            t64_virt_addr[0x1c/4]= 0xffffffff;
            t64_virt_addr[0x20/4]= 0x80;
            t64_virt_addr[0x24/4]= 0x03; //go
            t64_calibrate(100000);
            break;
        case(1):
            //stop
            t64_virt_addr[0x24/4]= 0x00;
            break;
        case(2):
            //read
            tv_lo= t64_virt_addr[0x10/4];
            tv_hi= t64_virt_addr[0x14/4];
            t1=timing_start();
            blah = read_t64();
            t2=timing_stop();

            printf("t64 = %x%x   %llx (read_t64 takes %d ticks)\n",tv_hi,tv_lo,blah, t2-t1);
            break;
    }
    
    if(munmap(map_base, MAP_SIZE) == -1)
        exit(-99);
    close(fd);
    return 0;
}
#endif
#endif
