/******************************************************************************
 * FILE PURPOSE:  Main Routines for HPLIB Shared Memory APIs
 ******************************************************************************
 * FILE NAME:   hplib_shem.c
 *
 * The HPLIB provides a set of utility APIs which provide the following functionality
 *
 * REVISION HISTORY:
 *
 *  Copyright (c) Texas Instruments Incorporated 2013
 * 
 *  Redistribution and use in source and binary forms, with or without 
 *  modification, are permitted provided that the following conditions 
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the   
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <errno.h>
#include <unistd.h>
#include <sys/ioctl.h>
#include "hplibmod.h"
#include "hplib.h"
#include "hplib_shm.h"
#include "hplib_loc.h"
#include <pthread.h>
#include <sched.h>
#include <linux/unistd.h>
#include <sys/syscall.h>
#include <sys/sysinfo.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/shm.h>

static int shm_key = 100;
//static int shm_size = 8192;
static int shm_id = 0;


/**************************************************************************
 *  FUNCTION PURPOSE:  Create Shared Memory Region 
 **************************************************************************/
void* hplib_shmCreate(int size)
{
    hplib_shmInfo_T* pBase;
    struct shmid_ds info;
    shm_id = shmget(shm_key, size, IPC_CREAT | 0666 );

    if (shm_id < 0)
    {
        hplib_Log("hplib_shmCreate: shmget error\n");
        return NULL;
    }
    else
    {
        pBase = (hplib_shmInfo_T*)shmat(shm_id, 0,0);
        if (pBase == (hplib_shmInfo_T*)-1)
            hplib_Log("hplib_shmCreate: shmat error\n");
        else
        {
            pBase->size = size;
            pBase->free_offset = ((sizeof(hplib_shmInfo_T) +127)/128)*128;
        }
        shmctl(shm_id, IPC_STAT, &info);

        return pBase;
    }
}

/**************************************************************************
 *  FUNCTION PURPOSE:  Open previously created Shared Memory Region
 **************************************************************************/
void* hplib_shmOpen(void)
{
//    void* pBase;
    hplib_shmInfo_T* pBase;
    //hplib_shmInfo_T* pShmBase;
    struct shmid_ds info;
    shm_id = shmget(shm_key, 0, 0666 );
    if (shm_id < 0)
    {
        hplib_Log("hplib_shmOpen: shmget error\n");
        return NULL;
    }
    else
    {
        shmctl(shm_id, IPC_STAT, &info);

        /* shmat(shmid, addr, flag), addr 0 implies system chooses suitable
           unused address at to which the attach the memory segment, flag =0 
           implies read/write access to the memory segment */
        //pBase = shmat(shm_id, 0,0);
        pBase = (hplib_shmInfo_T*)shmat(shm_id, 0,0);
        if (pBase == (hplib_shmInfo_T*)-1)
        {
            hplib_Log("hplib_shmOpen: shmat error\n");
            return NULL;
        }

        return pBase;
    }
}

/**************************************************************************
 *  FUNCTION PURPOSE:  Delete Shared Memory Segment
 **************************************************************************/
hplib_RetValue hplib_shmDelete(void)
{
    shm_id = shmget(shm_key, 0, 0666 );
    if (shm_id < 0)
    {
        hplib_Log("hplib_shmOpen: shmget error\n");
        return hplib_FAILURE;
    }
    else
    {
        int err=shmctl(shm_id, IPC_RMID, 0);
        if(err<0)
        {
            hplib_Log("hplib_shmDelete: shmctl failed\n");
            return hplib_FAILURE;
        }
    }
    return hplib_OK;
}

/**************************************************************************
 *  FUNCTION PURPOSE:  Add entry to Shared Memory Layout
 **************************************************************************/
hplib_RetValue hplib_shmAddEntry(void* base, int size, hplib_shmEntryId_E entryId)
{
    hplib_shmInfo_T* pBase = (hplib_shmInfo_T*) base;

    if ((pBase->free_offset + size) > pBase->size)
    {
        hplib_Log("hplib_shmAddEntry: insufficient memory request, size %d\n", size);
        return hplib_ERR_NOMEM;
    }

    if (pBase->toc[entryId] !=0)
    {
        return hplib_OK;
    }

    pBase->toc[entryId] = pBase->free_offset;
    pBase->free_offset += ((size +127)/128)*128;
    
    return hplib_OK;
}

void* hplib_shmGetEntry(void* base, hplib_shmEntryId_E entryId)
{
    hplib_shmInfo_T* pBase = (hplib_shmInfo_T*) base;
    return(base + pBase->toc[entryId]);
    
}


