/******************************************************************************
 * FILE PURPOSE:  Main Routines for HPLIB Virtual Memory Allocator
 ******************************************************************************
 * FILE NAME:   hplib_vm.c
 *
 * DESCRIPTION: The virtual memory APIs provide the following functionality:
 *       Maps peripheral registers into user space for LLDs. 
 *          QMSS
 *          CPPI
 *          SRIO
.*          PASS
 *          Timer64 (Appleton)
 *      Memory allocation routines.
 *      Routines to perform address translations (physical to virtual, virtual to physical)

 *
 * REVISION HISTORY:
 *
 *  Copyright (c) Texas Instruments Incorporated 2010-2012
 * 
 *  Redistribution and use in source and binary forms, with or without 
 *  modification, are permitted provided that the following conditions 
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the   
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <errno.h>
#include <unistd.h>
#include <sys/ioctl.h>
#include "hplibmod.h"
#include "hplib.h"
#include "ti/csl/cslr_device.h"

extern void* Osal_hplibCsEnter(void);
extern void Osal_hplibCsExit (void *CsHandle);

/***********************RAW MEMORY ALLOCATION & TRANSLATION*************************/
/* Macro to align x to y */
#define align(x,y)   ((x + y) & (~(y-1)))

#define HPLIB_VM_BM_ALLOC_PAGE_SIZE 256

hplib_virtualMemPoolAddr_T memPoolAddr[HPLIB_MAX_MEM_POOLS];
uint8_t *hplib_VM_mem_start_phy = (uint8_t*)0;
uint8_t *hplib_VM_mem_start = (uint8_t*)0;
uint8_t *hplib_VM_mem_end = (uint8_t*)0;
uint8_t *hplib_VM_mem_end_phy = (uint8_t*)0;
static uint8_t *hplib_VM_mem_alloc_ptr = (uint8_t*)0;
static uint32_t hplib_VM_mem_size = 0;
uint32_t hplib_VM_virt_to_phy_mapping;
uint32_t hplib_VM_phy_to_virt_mapping;

/* File descriptor for /dev/mem */ 
static int dev_mem_fd;

/**************************************************************************
 *  FUNCTION PURPOSE:   Maps the give physical address to virtual memory space
 **************************************************************************/
void *hplib_VM_MemMap
(
    void        *addr, /* Physical address */
    uint32_t    size   /* Size of block */
)
{
    void            *map_base,*virt_addr;
    uint32_t        page_sz;
    long            retval;

    retval = sysconf(_SC_PAGE_SIZE);
    if (retval == -1)
    {
        hplib_Log("hplib_VM_MemMap: Failed to get page size err=%s\n",
               strerror(errno));
        return (void *)0;
    }

    page_sz = (uint32_t)retval;

    if (size%page_sz)
    {
        hplib_Log("hplib_VM_MemMap: error: block size not aligned to page size\n");
        return (void *)0;
    }

    if ((uint32_t)addr%page_sz)
    {
        hplib_Log("hplib_VM_MemMap: error: addr not aligned to page size\n");
        return (void *)0;
    }

    map_base = mmap(0, size, (PROT_READ|PROT_WRITE), MAP_SHARED, dev_mem_fd, (off_t)addr);
    if(map_base == (void *) -1) 
    {
        hplib_Log("hplib_VM_MemMap: Failed to mmap \"dev/mem\" err=%s\n",
               strerror(errno));
        return (void *)0;
    }
    virt_addr = map_base;

    return(virt_addr);
}

unsigned long peek(unsigned long * p)
{
  return *p;
}

/**************************************************************************
 *  FUNCTION PURPOSE:   Initialize the allocated memory pool area 
 **************************************************************************/
HPLIB_BOOL_T hplib_VM_MemAllocInit
(
    uint8_t     *addr,      /* Physical address */
    uint32_t    size,        /* Size of block */
    uint8_t i      /* pool id */
)
{
    void *map_base = NULL;
    uint32_t mapSize = 0;
    uint32_t virtPoolHdrSize = sizeof(hplib_VirtMemPoolheader_T);

    /* Set up memory mapping for un-cached  memory, this requires physical  address and size of memory region to map*/
    if ((addr != NULL ) && size)
    {
        map_base = hplib_VM_MemMap((void *)addr, size);
        if (!map_base)
        {
            hplib_Log("hplib_VM_MemAllocInit(): Failed to memory map for uncached memory, addr (0x%x)", 
                        (uint32_t)addr);
            return HPLIB_FALSE;
        }

        memPoolAddr[i].memPoolHdr = (hplib_VirtMemPoolheader_T*)map_base;
        memPoolAddr[i].memStart = (uint8_t*)map_base + virtPoolHdrSize;

        memPoolAddr[i].memSize = size;
        memPoolAddr[i].memEnd = (uint8_t*)map_base + size;
        memPoolAddr[i].memStartPhy = addr + virtPoolHdrSize;
        memPoolAddr[i].memEndPhy = addr + size;
        mapSize = memPoolAddr[i].memSize / (HPLIB_VM_BM_ALLOC_PAGE_SIZE*8);//in bytes!

        memPoolAddr[i].memAllocPtr = memPoolAddr[i].memStart +
                                                    mapSize +
                                                    virtPoolHdrSize;
        memPoolAddr[i].memAllocPtr =
             (uint8_t*)align((uint32_t)memPoolAddr[i].memAllocPtr,
                            HPLIB_VM_BM_ALLOC_PAGE_SIZE);
        memPoolAddr[i].virtMapInfo =
              (uint8_t*)bmAllocInit(memPoolAddr[i].memAllocPtr,
               memPoolAddr[i].memSize - mapSize - virtPoolHdrSize,
               (uint32_t*)&memPoolAddr[i].memPoolHdr->bmap,
               HPLIB_VM_BM_ALLOC_PAGE_SIZE);
    }
    else if (addr == NULL)
    {
        addr=  ( uint8_t *) hplib_utilGetPhysOfBufferArea();
        size = hplib_utilGetSizeOfBufferArea();
        map_base = (void *) hplib_utilGetVaOfBufferArea(HPLIBMOD_MMAP_DMA_MEM_OFFSET,
                                                        size); //mmap into our space, return va
        memPoolAddr[i].memPoolHdr = (hplib_VirtMemPoolheader_T*)map_base;
        memPoolAddr[i].memStart = (uint8_t*)map_base;
        memPoolAddr[i].memSize =size;
        memPoolAddr[i].memEnd = (uint8_t*)map_base + size;
        memPoolAddr[i].memStartPhy = addr;
        memPoolAddr[i].memEndPhy = addr + size;
        mapSize = memPoolAddr[i].memSize / (HPLIB_VM_BM_ALLOC_PAGE_SIZE*8);

        memPoolAddr[i].memAllocPtr = memPoolAddr[i].memStart +
                                                    mapSize +
                                                    virtPoolHdrSize;

         memPoolAddr[i].memAllocPtr =
             (uint8_t*)align((uint32_t)memPoolAddr[i].memAllocPtr,
                            HPLIB_VM_BM_ALLOC_PAGE_SIZE);

        memPoolAddr[i].virtMapInfo=
              (uint8_t*)bmAllocInit(memPoolAddr[i].memAllocPtr,
               memPoolAddr[i].memSize - mapSize - virtPoolHdrSize,
               (uint32_t*)&memPoolAddr[i].memPoolHdr->bmap,
               HPLIB_VM_BM_ALLOC_PAGE_SIZE);
    }
    else
    {
        return HPLIB_FALSE;
    }
    if (i== 0)
    {
        hplib_VM_mem_size = size;
        hplib_VM_mem_alloc_ptr = hplib_VM_mem_start = map_base +
                                                      mapSize +
                                                      virtPoolHdrSize;
        hplib_VM_mem_end = map_base + hplib_VM_mem_size;
        hplib_VM_mem_start_phy = addr + mapSize + virtPoolHdrSize;
        hplib_VM_mem_end_phy = addr + hplib_VM_mem_size;
        hplib_VM_virt_to_phy_mapping = (uint32_t) hplib_VM_mem_start_phy - (uint32_t)hplib_VM_mem_start;
        hplib_VM_phy_to_virt_mapping = (uint32_t)hplib_VM_mem_start - (uint32_t)hplib_VM_mem_start_phy;
    }
    return HPLIB_TRUE;
}



/**************************************************************************
 *  FUNCTION PURPOSE:   Returns the free amount of buffer/descriptor area
 *  for the memory pool specified.
 **************************************************************************/

uint32_t  hplib_vmGetMemPoolRemainder(int pool_id)
{
    hplib_VirtMemPoolheader_T *poolHdr;
    uint32_t remainder;
    void* key;
    uint32_t virtPoolHdrSize;
    uint32_t mapSize;

    if (pool_id > (HPLIB_MAX_MEM_POOLS -1))
        return 0;
    Osal_hplibCsEnter();

    poolHdr = (hplib_VirtMemPoolheader_T*) memPoolAddr[pool_id].memStart;

    mapSize = memPoolAddr[pool_id].memSize / (HPLIB_VM_BM_ALLOC_PAGE_SIZE*32);
    virtPoolHdrSize = sizeof(hplib_VirtMemPoolheader_T);
    Osal_hplibCsExit(key);

    return (memPoolAddr[pool_id].memEnd -
                memPoolAddr[pool_id].memStart -
                mapSize -
                virtPoolHdrSize -
                poolHdr->totalAllocated);

}

/*****************************************************************************
 * FUNCTION PURPOSE: Free memory for Buffer/Descriptors from memory pool
 *                   specified
 *****************************************************************************/
void hplib_vmMemFree
(
    void        *ptr,
    uint32_t    size,
    int         pool_id
)
{
    void* key = NULL;
    hplib_VirtMemPoolheader_T *poolHdr;

    if ((pool_id > (HPLIB_MAX_MEM_POOLS -1)) || (ptr == NULL))
        return;
    Osal_hplibCsEnter();

    poolHdr = (hplib_VirtMemPoolheader_T*) memPoolAddr[pool_id].memStart;

    bmFree(memPoolAddr[pool_id].virtMapInfo,(unsigned char*)ptr, size);
    poolHdr->totalAllocated -=size;

    Osal_hplibCsExit(key);
    return;
}
/*****************************************************************************
 * FUNCTION PURPOSE: Allocates memory for Buffer/Descriptors from memory pool specified 
 *****************************************************************************/
void* hplib_vmMemAlloc
(
    uint32_t    size,
    uint32_t    alignment,
    int         pool_id
)
{

    void* key = NULL;
    hplib_VirtMemPoolheader_T *poolHdr;
    void *pMalloc = NULL;

    if (pool_id > (HPLIB_MAX_MEM_POOLS -1))
    {
        return NULL;
    }
    
    Osal_hplibCsEnter();
    poolHdr = (hplib_VirtMemPoolheader_T*) memPoolAddr[pool_id].memStart;

    pMalloc = bmAlloc(memPoolAddr[pool_id].virtMapInfo,size);
    if (pMalloc)
        poolHdr->totalAllocated += size;

    Osal_hplibCsExit(key);
    return pMalloc;
}


/********************************************************************
 *FUNCTION PURPOSE: The function API is used to release/unmap continuous 
 *  block of memory allocated via  hplib_VM_MemorySetup function, 
 *  remove mapping of virtual memory for peripherals.
 ********************************************************************/
void hplib_vmTeardown(void)
{
    hplib_utilModClose();
    close(dev_mem_fd);
}

hplib_RetValue hplib_checkMallocArea(int pool_id)
{
    hplib_VirtMemPoolheader_T *poolHdr;
    if (pool_id > (HPLIB_MAX_MEM_POOLS-1))
    {
        return hplib_FAILURE;
    }
    poolHdr = (hplib_VirtMemPoolheader_T*) memPoolAddr[pool_id].memStart;
    if(poolHdr->ref_count)
        return hplib_OK;
    else
        return hplib_FAILURE;
}

hplib_RetValue hplib_resetMallocArea(int pool_id)
{
    hplib_VirtMemPoolheader_T *poolHdr;
    if (pool_id > (HPLIB_MAX_MEM_POOLS-1))
    {
        return hplib_FAILURE;
    }
    poolHdr = (hplib_VirtMemPoolheader_T*) memPoolAddr[pool_id].memStart;

    bmAllocClose(memPoolAddr[pool_id].virtMapInfo);

    memset(poolHdr,
           0,
           memPoolAddr[pool_id].memAllocPtr - memPoolAddr[pool_id].memStart);

    return hplib_OK;
}

hplib_RetValue hplib_initMallocArea(int pool_id)
{
    hplib_VirtMemPoolheader_T *poolHdr;

    if (pool_id > (HPLIB_MAX_MEM_POOLS -1))
    {
        return hplib_FAILURE;
    }

    poolHdr = (hplib_VirtMemPoolheader_T*) memPoolAddr[pool_id].memStart;

    poolHdr->ref_count++;
    poolHdr->totalAllocated = 0;

    return hplib_OK;

}
/********************************************************************
 * FUNCTION PURPOSE: Allocate continuous block of cached memory via CMA and
 * optionally un-cached memory if specified, maps virtual memory for peripheral registers.
 ********************************************************************/
hplib_RetValue hplib_vmInit(hplib_virtualAddrInfo_T *hplib_vmaddr,
                            int num_pools,
                            hplib_memPoolAttr_T *mempool_attr)
{
    int i;
    
    if (num_pools == 0)
    {
        hplib_Log("hplib_vmInit(): Error, invalid number of pools\n");
        return -1;
    }


    /*Open dev/mem, since we need for QM, CPPI, etc */
    if((dev_mem_fd = open("/dev/mem", (O_RDWR | O_SYNC))) == -1)
    {
        hplib_Log("hplib_vmInit: Failed to open \"dev/mem\" err=%s\n",
               strerror(errno));
        return HPLIB_FALSE;
    }

    /* Open kernel module since we need it for PA to VA mappings */
    if (hplib_utilModOpen() == -1)
    {
        hplib_Log("hplib_vmInit:: failed to open /dev/netapi: '%s'\n", strerror(errno));
        return HPLIB_FALSE;
    }
    for (i=0;i < num_pools; i++)
    {
        /* Initialize memory which was allocated from DDR via hplibmod CMA*/
        if (mempool_attr[i].attr == HPLIB_ATTR_KM_CACHED0) {
            if (hplib_VM_MemAllocInit( NULL, 0, i) == HPLIB_FALSE)
            {
                hplib_Log("hplib_vmInit: hplib_VM_MemAllocInit from DDR/CMA failed\n");
                return hplib_ERR_NOMEM;
            }
        }
        /* Intialize memory for un-cached memory, make sure size and phys_addr passed in and have valid values */
        if ((mempool_attr[i].attr == HPLIB_ATTR_UN_CACHED) && mempool_attr[i].size && mempool_attr[i].phys_addr)
        {
            if (hplib_VM_MemAllocInit((uint8_t*)mempool_attr[i].phys_addr,
                                                            mempool_attr[i].size, i) == HPLIB_FALSE)
            {
                hplib_Log(" hplib_vmInit: hplib_VM_MemAllocInit from un-cached memory failed\n");
                return hplib_ERR_NOMEM;
            }
        }
    }

    /* Create virtual memory maps for peripherals */
    /* QMSS CFG Regs */
    hplib_vmaddr->qmssCfgVaddr =
        hplib_VM_MemMap((void*)CSL_QMSS_CFG_BASE, QMSS_CFG_BLK_SZ);
    if (!hplib_vmaddr->qmssCfgVaddr)
    {
        hplib_Log("hplib_vmInit: Failed to map QMSS CFG registers\n");
        return hplib_FAILURE;
    }

    /*QMSS DATA Regs */
    hplib_vmaddr->qmssDataVaddr =
        (void *) hplib_utilGetVaOfBufferArea(HPLIBMOD_MMAP_QM_DATA_REG_MEM_OFFSET,
                                             QMSS_DATA_BLK_SZ);

    if (!hplib_vmaddr->qmssDataVaddr)
    {
        hplib_Log("hplib_vmInit(): Failed to map QMSS DATA registers\n");
        return hplib_FAILURE;
    }


    /* PASS CFG Regs */
    hplib_vmaddr->passCfgVaddr =
            hplib_VM_MemMap((void*)CSL_NETCP_CFG_REGS,
                                   //p_device_cfg->netcpCfgBlkSz
                                   PASS_CFG_BLK_SZ);

    if (!hplib_vmaddr->passCfgVaddr)
    {
        hplib_Log("hplib_vmInit(): Failed to map PASS CFG registers\n");
        return hplib_FAILURE;
    }

#ifdef CORTEX_A8
    /* (2f) Timer */
    t64_memmap(dev_mem_fd);
#endif

    return hplib_OK;

}

