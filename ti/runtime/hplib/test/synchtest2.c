/**************************************
 * file : synchtest2.c
 **************************************
 * * FILE:  synchtest2.c
 * 
 * DESCRIPTION:  netapi user space transport
 *               library  test application - more synchtest
 * 
 * REVISION HISTORY:  rev 0.0.1 
 *
 *  Copyright (c) Texas Instruments Incorporated 2010-2011
 * 
 *  Redistribution and use in source and binary forms, with or without 
 *  modification, are permitted provided that the following conditions 
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the   
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************/

#include <stdio.h>
#include <pthread.h>
#include <unistd.h>
#include <stdlib.h>
#include <sched.h>
#include <linux/unistd.h>
#include <sys/syscall.h>
#include <sys/sysinfo.h>
#include <errno.h>
#include "hplib.h"


//#define INC_TO 1000000 // one million.
#define INC_TO 10000 // one million...
#define timing_start hplib_mUtilGetPmuCCNT
#define timing_stop hplib_mUtilGetPmuCCNT


#define SYNCHTEST2_ENTRY 61

struct synchtest_info_t
{
    hplib_atomic64_T global_int;
    hplib_spinLock_T spin_test;
    hplib_spinLock_T setup_lock;
    int blah2;
    int sync_flag;
};

#define SHM_SIZE 65536
__thread blah=0;


hplib_shmInfo_T* pshmBase;

void *thread_routine( void *arg )
{
    int i;
    int proc_num = (int)(long)arg;
    cpu_set_t set;
    unsigned long long t1, t2;
    struct synchtest_info_t* pSynchTest2Info;
    void* pshmBase;

    pshmBase = (hplib_shmInfo_T*)hplib_shmOpen();
    pSynchTest2Info = (struct synchtest_info_t*)hplib_shmGetEntry((void*)pshmBase,SYNCHTEST2_ENTRY);

    hplib_mSpinLockInit(&pSynchTest2Info->setup_lock);
    hplib_mSpinLockLockMP(&pSynchTest2Info->setup_lock);
    CPU_ZERO( &set);
#ifdef CORTEX_A8
    CPU_SET( 0, &set);
    hplib_utilSetupThread(proc_num, &set, hplib_spinLock_Type_LOL);
#else
    CPU_SET( proc_num, &set);
    hplib_utilSetupThread(proc_num, &set, hplib_spinLock_Type_LOL);
#endif


    hplib_mSpinLockUnlockMP(&pSynchTest2Info->setup_lock);


    t1 = timing_start();
    for (i = 0; i < INC_TO; i++)
    {
        //hplib_mAtomic64Add(&global_int,1LL);
        hplib_mAtomic64Add(&pSynchTest2Info->global_int, 1LL);
        blah+=1;
        //blah2+=1;
        pSynchTest2Info->blah2+=1;
    }
    printf("cpu[%d]:     thread %d, blah=%d\n", sched_getcpu(), proc_num, blah);
    printf("cpu[%d]:     thread %d, calling hplib_spinLock_lock(), about to grab lock \n", sched_getcpu(), proc_num);

    //now we try the synch_lock_and_test
    //hplib_mSpinLockLockMP(&spin_test);
    hplib_mSpinLockLockMP(&pSynchTest2Info->spin_test);

    printf("cpu[%d]:     thread %d, got the lock,  lock val= %d\n",sched_getcpu(), proc_num,pSynchTest2Info->spin_test);
    sleep(2);
    printf("cpu[%d]:     thread %d, i'm done sleeping, i'm unlocking now\n",sched_getcpu(), proc_num); 
    //hplib_mSpinLockUnlockMP(&spin_test);
    hplib_mSpinLockUnlockMP(&pSynchTest2Info->spin_test);

    t2 = timing_stop();
    printf("cpu[%d]:     arm start time: %lld, arm end time: %lld, arm elapsed time: %lld\n", sched_getcpu(), t1, t2, t2-t1);

    return NULL;
}

main(int argc, char * argv[])
{
    int procs = 0;
    int procs_conf;
    int procs_avail;
    int i, retVal;
    int op=0;
    pthread_t *thrs;
    printf("at start up\n");
    struct synchtest_info_t* pSynchTest2Info;
    
    if (argc<2) {printf("synchtest2  (c[reate]| d[delete]| o[open])\n"); exit(1);}

    switch (argv[1][0])
    {
        case 'd':
        case 'D':
            op =2; //delete
            break;
        case 'c':
        case 'C':
            op =0; //create
            break;
        case 'o':
        case 'O':
            op =1; //open
            break;
        default:
            printf(" unknown op code %c.  Need d, o, or c\n", argv[1][0]);
            exit(1);
        break;
    }
    printf("op =%d\n", op);
    procs_avail= get_nprocs();
    procs_conf = get_nprocs_conf();



    if (op ==0)
    {
        /* create shared memory region */
        pshmBase = (hplib_shmInfo_T*)hplib_shmCreate(SHM_SIZE);
        if(pshmBase == -1)
        {
            printf("main: hplib_shmCreate: failure\n");
        }
        else
        {
            printf("main: hplib_shmCreate: sucess\n");
            printf("size of free area is now %d\n", pshmBase->free_offset);
            hplib_shmAddEntry((void*) pshmBase,
                               sizeof(struct synchtest_info_t),
                               SYNCHTEST2_ENTRY);
            pSynchTest2Info = 
                (struct synchtest_info_t*)hplib_shmGetEntry((void*)pshmBase,SYNCHTEST2_ENTRY);

            pSynchTest2Info->blah2 = 0;
            
            pSynchTest2Info->global_int.val = 0LL;
            pSynchTest2Info->global_int.lock = hplib_spinLock_UNLOCKVAL;

            pSynchTest2Info->setup_lock = hplib_spinLock_UNLOCKED_INITIALIZER;
            pSynchTest2Info->spin_test = hplib_spinLock_UNLOCKED_INITIALIZER;
            pSynchTest2Info->sync_flag = 1;
            sleep(15);
            //pSynchTest2Info->sync_flag = 0;
        }
        
    }
    /* open shared memory segment */
    else if(op == 1)
    {
        pshmBase = (hplib_shmInfo_T*)hplib_shmOpen();
        if (pshmBase == NULL)
        {
            printf("main: hplib_shmOpen failure\n");
            exit(1);
        }
            
        printf("pshmBase size %d, free %d\n",
                pshmBase->size,
                pshmBase->free_offset);

        pSynchTest2Info = (struct synchtest_info_t*)hplib_shmGetEntry((void*)pshmBase,SYNCHTEST2_ENTRY);
        printf("blah2 = %d\n", pSynchTest2Info->blah2);
        while((volatile)pSynchTest2Info->sync_flag == 0);
#if 0
        {
            printf("sync_flag %d\n", pSynchTest2Info->sync_flag);
        }
#endif
    }
    else if (op == 2)
    {
        if ((retVal = hplib_shmDelete()) != hplib_OK)
            printf("main: hplib_shmDelete failure\n");
        else
        {
            printf("main: hplib_shmDelete sucess\n");
            exit(1);
        }
        
    }

    //return 0;
    
    printf("procs_avail: %d, procs_conf: %d\n", procs_avail, procs_conf);
    
    // Getting number of CPUs
    procs = (int)sysconf( _SC_NPROCESSORS_ONLN );
    if (procs < 0)
    {
        perror( "sysconf" );
        return -1;
    }
    printf(" num cpus = %d\n",procs);
    
    if (procs==1)
    {
        procs+=1;
        printf("adding 2nd 'core' \n");
    }
    thrs = malloc( sizeof( pthread_t ) * procs *2);
    if (thrs == NULL)
    {
        perror( "malloc" );
        return -1;
    }

    //initialize spinLock
    hplib_mSpinLockInit(&pSynchTest2Info->spin_test);
    printf( "Starting %d threads...\n", procs );

    for (i = 0; i < procs; i++)
    {
        if (pthread_create( &thrs[i], NULL, thread_routine,
        (void *)(long)i ))
        {
            perror( "pthread_create" );
            procs = i;
            break;
        }
    }


    for (i = 0; i < procs; i++)
        pthread_join( thrs[i], NULL );

    free( thrs );

    printf( "atomic int value: %lld, non-atomic int value: %d\n",
    pSynchTest2Info->global_int.val, pSynchTest2Info->blah2 );
    printf( "Expected value for atomic : %d\n", INC_TO * procs);
    if (pSynchTest2Info->global_int.val == (INC_TO *procs*2))
        printf("synchtest2: PASSED\n");
    else
        printf("synchtest2: FAILED\n");

    return 0;
}
