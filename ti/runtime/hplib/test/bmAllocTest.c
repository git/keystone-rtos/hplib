/*************************************************
* FILE:  shmtest.c
 * 
 * DESCRIPTION:  netapi user space transport
 *               library  test application (not used, kept as refenence)
 * 
 * REVISION HISTORY:  rev 0.0.1 
 *
 *  Copyright (c) Texas Instruments Incorporated 2010-2011
 * 
 *  Redistribution and use in source and binary forms, with or without 
 *  modification, are permitted provided that the following conditions 
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the   
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 **********************************************************************/


#include <stdlib.h>
#include <sys/shm.h>
#include <errno.h>
#include "hplib.h"
#include <sched.h>
#include <linux/unistd.h>
#include <sys/syscall.h>
#include <sys/sysinfo.h>

hplib_memPoolAttr_T netapi_VM_MempoolAttr[HPLIB_MAX_MEM_POOLS];
hplib_virtualAddrInfo_T netapi_VM_VirtAddr[HPLIB_MAX_MEM_POOLS];


#define SHM_SIZE 65536
#define MAX_ALLOCS 1000
#define ALLOC_SIZE 256
char * p= NULL;

static void* thread_routineM(void* arg)
{
    uint32_t i;
    char * p= NULL;
    void* ptr[MAX_ALLOCS];
    cpu_set_t cpu_set;
    hplib_VirtMemPoolheader_T *poolHdr;
    poolHdr = (hplib_VirtMemPoolheader_T*) memPoolAddr[0].memStart;

    p = hplib_shmCreate(SHM_SIZE);
    if(p == NULL)
    {
        printf("main: hplib_shmCreate failed\n");
        exit(0);
    }
    
    hplib_utilOsalCreate();
    CPU_ZERO( &cpu_set);
    CPU_SET( 0, &cpu_set);

    hplib_utilSetupThread(1, &cpu_set, hplib_spinLock_Type_LOL);

    hplib_vmInit(&netapi_VM_VirtAddr[0],
                  1,
                  &netapi_VM_MempoolAttr[0]);
    hplib_initMallocArea(0);
    poolHdr = (hplib_VirtMemPoolheader_T*) memPoolAddr[0].memStart;
    for (i=0;i < MAX_ALLOCS;i ++)
    {
        ptr[i] = hplib_vmMemAlloc(ALLOC_SIZE,128,0);
        if (ptr[i] == NULL)
            printf("Null ptr\n");
        else
            printf("ptr[%d]: 0x%x, totalAllocatedSoFar %d\n",
            i,
            ptr[i],
            poolHdr->totalAllocated);
          
    }
    sleep(60);
    for (i=0;i < MAX_ALLOCS;i ++)
    {
        if(ptr[i])
        {
            hplib_vmMemFree(ptr[i], ALLOC_SIZE,0);
            printf("now freeing: totalAllocatedSoFar %d\n",
            poolHdr->totalAllocated);
        }
    }
    hplib_resetMallocArea(0);
    hplib_vmTeardown();

}
static void* thread_routineP(void* arg)
{
    uint32_t i;
    char * pShmBase= NULL;
    void* ptr[MAX_ALLOCS*2];
    cpu_set_t cpu_set;
    hplib_VirtMemPoolheader_T *poolHdr;

    pShmBase = hplib_shmOpen();
    if(pShmBase == NULL)
    {
        printf("main: hplib_shmCreate failed\n");
        exit(0);
    }

    CPU_ZERO( &cpu_set);
    CPU_SET( 0, &cpu_set);

    hplib_utilSetupThread(2, &cpu_set, hplib_spinLock_Type_LOL);
    Osal_start(pShmBase);

    hplib_vmInit(&netapi_VM_VirtAddr[0],
                  1,
                  &netapi_VM_MempoolAttr[0]);
    printf("awake now: doing allocs\n");
    poolHdr = (hplib_VirtMemPoolheader_T*) memPoolAddr[0].memStart;
    printf("MapInfo: after allocs, totalAllocatedBytes: %d\n", 
        poolHdr->totalAllocated);
    for (i=0;i < MAX_ALLOCS*2;i ++)
    {
        ptr[i] = hplib_vmMemAlloc(ALLOC_SIZE,128,0);
        if (ptr[i] == NULL)
            printf("Null ptr\n");
        else
            printf("ptr[%d]: 0x%x, totalAllocatedSoFar %d\n",
            i,
            ptr[i],
            poolHdr->totalAllocated);
          
    }
    for (i=0;i < MAX_ALLOCS*2;i ++)
    {
        if(ptr[i])
        {
            hplib_vmMemFree(ptr[i], ALLOC_SIZE,0);
            printf("now freeing: totalAllocatedSoFar %d\n",
            poolHdr->totalAllocated);
        }

    }
}


main(int argc, char * argv[])
{
    int fd, i;
    int op=0;
    key_t key = 0xfeed;
    int err;
    void * map_base;
    int hplib_mod_fd;
    pthread_t *thrs;
    cpu_set_t cpu_set;

    /* Init attributes for DDR */

    netapi_VM_MempoolAttr[0].attr = HPLIB_ATTR_KM_CACHED0;
    netapi_VM_MempoolAttr[0].phys_addr = 0;
    netapi_VM_MempoolAttr[0].size = 0;

    if (argc<2) {printf("shmtest  (s[SystemM]| d[shutDown]| p[processM])  key\n"); exit(1);}
    /* assign main net_test thread to run on core 0 */
    CPU_ZERO( &cpu_set);
    CPU_SET( 0, &cpu_set);
    hplib_utilSetupThread(0, &cpu_set, hplib_spinLock_Type_LOL);

#if 1

    switch (argv[1][0])
    {
        case 'd':
        case 'D':
            op =2; //delete
            break;
        case 's':
        case 'S':
            op =0; //System master
            break;
        case 'p':
        case 'P':
            op =1; //Process Master
            break;
        default:
            printf(" unknown op code %c.  Need d, o, or c\n", argv[1][0]);
            exit(1);
        break;
    }

    switch (op)
    {
        case(1): /* process master */
        default:
            thrs = malloc( sizeof( pthread_t ));
            if (thrs == NULL)
            {
                perror( "malloc" );
                return -1;
            }
            if (pthread_create( &thrs[0], NULL, thread_routineP,
                (void *)(long)0 ))
            {
                perror( "pthread_create" );
                //procs = i;
                break;
            }
            pthread_join( thrs[0], NULL );
            break;


        case(0): /* System master */
            thrs = malloc( sizeof( pthread_t ));
            if (thrs == NULL)
            {
                perror( "malloc" );
                return -1;
            }
            if (pthread_create( &thrs[0], NULL, thread_routineM,
                (void *)(long)0 ))
            {
                perror( "pthread_create" );
                //procs = i;
                break;
            }
            pthread_join( thrs[0], NULL );

            break;

        case(2): /* Shutdown */
            hplib_shmDelete();
            hplib_mod_fd=hplib_utilModOpen();
            if (hplib_mod_fd == -1)
            {
                hplib_Log("main:: failed to open /dev/netapi: '%s'\n",
                           strerror(errno));
                exit(0);
            }
            map_base = (void *) hplib_utilGetVaOfBufferArea(HPLIBMOD_MMAP_DMA_MEM_OFFSET, 0);
            if (map_base)
            {
                hplib_resetMallocArea(0);
                hplib_vmTeardown();
                printf("netapi_shutdown: calling hplib_shmDelete\n");
                hplib_shmDelete();
            }
            close(hplib_mod_fd);
            exit( 0);
            break;
    }
#endif
}
