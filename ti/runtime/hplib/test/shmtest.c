/*************************************************
* FILE:  shmtest.c
 * 
 * DESCRIPTION:  netapi user space transport
 *               library  test application (not used, kept as refenence)
 * 
 * REVISION HISTORY:  rev 0.0.1 
 *
 *  Copyright (c) Texas Instruments Incorporated 2010-2011
 * 
 *  Redistribution and use in source and binary forms, with or without 
 *  modification, are permitted provided that the following conditions 
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the   
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 **********************************************************************/


#include <stdlib.h>
#include <sys/shm.h>
#include <errno.h>
#include "hplib.h"

#define SHM_SIZE 65536
 char * p= NULL;
int * pi=NULL;
main(int argc, char * argv[])
{
    int fd;
    int op=0;
    key_t key = 0xfeed;
    int err;
    void * map_base;
    int hplib_mod_fd;
    hplib_VirtMemPoolheader_T *poolHdr;
    
    if (argc<3) {printf("shmtest  (c[reate]| d[delete]| o[open])  key\n"); exit(1);}


    switch (argv[1][0])
    {
        case 'd':
        case 'D':
            op =2; //delete
            break;
        case 'c':
        case 'C':
            op =0; //create
            break;
        case 'o':
        case 'O':
            op =1; //open
            break;
        default:
            printf(" unknown op code %c.  Need d, o, or c\n", argv[1][0]);
            exit(1);
        break;
    }

    key = atoi(argv[2]); printf("key = %d op=%d\n", key,op);
    //size = atio(argv[3]); printf("size =%d\n", size);
    switch (op)
    {
        case(1): /* open */
        default:
            //fd = shmget(key, SHM_SIZE, 0666 );
            p = hplib_shmOpen();
            if(p == NULL)
            {
                printf("main: hplib_shmOpen failed\n");
                exit(0);
            }
            break;

        case(0): /* create */
            //fd = shmget(key, SHM_SIZE, IPC_CREAT | 0666 );
            p = hplib_shmCreate(SHM_SIZE);

            break;

        case(2): /* delete */
            //fd = shmget(key, SHM_SIZE, 0666 );
            hplib_shmDelete();
            hplib_mod_fd=hplib_utilModOpen();
            if (hplib_mod_fd == -1)
            {
                hplib_Log("main:: failed to open /dev/netapi: '%s'\n",
                           strerror(errno));
                return HPLIB_FALSE;
            }
            map_base = (void *) hplib_utilGetVaOfBufferArea(HPLIBMOD_MMAP_DMA_MEM_OFFSET, 0);
            if (map_base)
            {
                poolHdr = (hplib_VirtMemPoolheader_T*)map_base;
                poolHdr->ref_count = 0;
            }
            close(hplib_mod_fd);
            exit( 0);
            break;
    }
}
