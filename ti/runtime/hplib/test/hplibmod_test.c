#include <stdio.h>
#include <unistd.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/ioctl.h>
#include <fcntl.h>
#include <errno.h>
#include <string.h>

#include "hplibmod.h"
#include "hplib_util.h"

#define timing_start hplib_mUtilGetPmuCCNT
#define timing_stop hplib_mUtilGetPmuCCNT

//#define AVOID_MMAP
#ifdef AVOID_MMAP
static int fd;
#endif


#define __DEBUG

#ifdef __DEBUG
#define __D(fmt, args...) fprintf(stdout, "HPLIBMODTEST Debug: " fmt, ## args)
#else
#define __D(fmt, args...)
#endif

#define __E(fmt, args...) fprintf(stderr, "HPLIBMODTEST Error: " fmt, ## args)

//static int hplib_mod_fd;
extern int hplib_mod_fd;
int hplibmod_init(void)
{
    hplib_mod_fd = open("/dev/hplib", O_RDWR);

    if (hplib_mod_fd == -1) {
        __E("init: failed to open /dev/hplib: '%s'\n", strerror(errno));
        return -1;
    }

    __D("init: successfully open /dev/hplib.\n");
    return 0;
}

void hplibmod_close(void)
{
    close(hplib_mod_fd);

#ifdef AVOID_MMAP
    close(fd);
#endif
}


static inline unsigned long hplibmod_getPhys(void)
{
    unsigned long physp;
    /*hplib_utilGetPhysOfBufferArea */
    if (ioctl(hplib_mod_fd, HPLIBMOD_IOCGETPHYS | HPLIBMOD_IOCMAGIC, &physp) == -1) {
        __E("getPhys: Failed to get physical address.\n");
        return 0;
    }

    __D("getPhys: exiting, ioctl HPLIBMOD_IOCGETPHYS succeeded, returning %#llx\n",
        physp);

    return physp;
}

static inline unsigned long hplibmod_getSize(void)
{
    unsigned long size;
    /* hplib_utilGetSizeOfBufferArea */
    if (ioctl(hplib_mod_fd, HPLIBMOD_IOCGETSIZE | HPLIBMOD_IOCMAGIC, &size) == -1) {
        __E("getSize: Failed to get size.\n");
        return 0;
    }

    __D("getSize: exiting, ioctl HPLIBMOD_IOCGETSIZE succeeded, returning 0x%x\n", size);

    return size;
}

static inline int hplibmod_cacheWb(void *ptr, size_t size)
{
#if 0
    struct hplibmod_block block;

    __D("cacheWb: entered w/ addr %p, size %p\n", ptr, size);

    block.addr = (unsigned long)ptr;
    block.size = size;

    if (ioctl(hplib_mod_fd, HPLIBMOD_IOCCACHEWB | HPLIBMOD_IOCMAGIC, &block) == -1) {
        __E("cacheWb: Failed to writeback %p\n", ptr);

        return -1;
    }

    __D("cacheWb: exiting, ioctl HPLIBMOD_IOCCACHEWB succeeded, returning 0\n");
#endif
    return 0;
}

static inline int hplibmod_cacheWbInv(void *ptr, size_t size)
{
#if 0
    struct hplibmod_block block;

    __D("cacheWbInv: entered w/ addr %p, size %p\n", ptr, size);

    block.addr = (unsigned long)ptr;
    block.size = size;

    if (ioctl(hplib_mod_fd, HPLIBMOD_IOCCACHEWBINV | HPLIBMOD_IOCMAGIC, &block) == -1) {
        __E("cacheWbInv: Failed to writeback & invalidate %p\n", ptr);

        return -1;
    }

    __D("cacheWbInv: exiting, ioctl HPLIBMOD_IOCCACHEWBINV succeeded, returning 0\n");
#endif
    return 0;
}

static inline int hplibmod_cacheInv(void *ptr, size_t size)
{
#if 0
    struct hplibmod_block block;

    __D("cacheInv: entered w/ addr %p, size %p\n", ptr, size);

    block.addr = (unsigned long)ptr;
    block.size = size;

    if (ioctl(hplib_mod_fd, HPLIBMOD_IOCCACHEINV | HPLIBMOD_IOCMAGIC, &block) == -1) {
        __E("cacheInv: Failed to invalidate %p\n", ptr);

        return -1;
    }

    __D("cacheInv: exiting, ioctl HPLIBMOD_IOCCACHEINV succeeded, returning 0\n");

#endif
    return 0;
}

static inline unsigned long hplibmod_mmap(void * phys, int sz)
{
    void *userp;
#ifdef AVOID_MMAP
    /* linux kernel mmap */
    if(  (fd = open("/dev/mem", O_RDWR )) == -1) return (unsigned long) NULL;
    __D("/dev/mem opened.\n");

    /* Map sz bytes` */
    userp = mmap(0,sz , PROT_READ | PROT_WRITE, MAP_SHARED, fd, phys);
    if(userp == (void *) -1) exit(1);
    __D("Memory mapped at address %p sz=%d phys=%x. (cached=%d)\n", userp,1,sz,phy);

#else
    /* hplibmod.ko mmap */
    /* Map the physical address to user space */
    userp = mmap(0,                       // Preferred start address
                 sz,            // Length to be mapped
                 PROT_WRITE | PROT_READ,  // Read and write access
                 MAP_SHARED,              // Shared memory
                 hplib_mod_fd,               // File descriptor
                 0);                      // The byte offset from fd
#endif
    if (userp == MAP_FAILED) {
        __E("allocHeap: Failed to mmap.\n");
        return 0;
    }
    __D("mmap succeeded, returning virt buffer %p\n", userp);

    return (unsigned long)userp;
}

long buffer[10000];

int main()
{
    unsigned long size,size2;
    unsigned long physp, virtp;
    unsigned long physp2, virtp2;
    int * pTest;
    int i,j,k;
    register unsigned long v1;
    register unsigned long v2;
    register unsigned long v3;
    register unsigned long v4;
    register long sum=0;
    register long *p;
    int a[4];
    int c;
    cpu_set_t set;


    CPU_ZERO( &set);
    CPU_SET( 0, &set);
    hplib_utilSetupThread(0, &set, hplib_spinLock_Type_LOL);
    v1 = timing_start();
    sleep(1);
    v2 = timing_stop();
    printf("PMU time calibrate :  1 sec = %d tics\n", v2-v1);
    

    a[0] = 0xa;
    a[1] = 0xb;
    a[2] = 0xc;
    a[3] = 0xd;
    if (hplibmod_init()) return -1;
    if (!(physp = hplibmod_getPhys())) return -1;
    if (!(size = hplibmod_getSize())) return -1;
    v1 = timing_start();

#if 0
    for(i=0;i<10000;i++)
    {
    if (!(size = hplibmod_getSize())) return -1;
    }
    v2 = timing_stop();
    __D("getSize: (pure ioctl)  %d cycles \n", (v2-v1)/10000 );
    sleep(1); 
#endif
    if (!(virtp = hplibmod_mmap(physp,size))) return 1;
    __D("virtp = %x phys=%x sz=%d\n",virtp, physp, size) ;
    //__D("virtp = %#llx phys=%#llx sz=%d\n",virtp, physp, size);

    pTest = (int*)virtp;

    //QM  data registers
    size2=1048576;
    physp2=0x44020000;
    virtp2 = mmap(0,                       // Preferred start address
                 size2,            // Length to be mapped
                 PROT_WRITE | PROT_READ,  // Read and write access
                 MAP_SHARED,              // Shared memory
                 hplib_mod_fd,               // File descriptor
                 physp2);
    __D("QM Data registers virtp = %x phys=%x sz=%d\n",virtp2, physp2, size2) ;

    /* now lets access the memory */
    memcpy(pTest, &a[0],sizeof(a));
    for (i=0;i < 4;i++)
    {
        printf(" printing from memory, virt[%d]: 0x%x\n", i,*pTest);
        pTest++;
        
    }
    sleep(1); 

#if 0
    p = (long*) virtp;
    v1 = timing_start();
    for(i=0;i<10000;i++)
    {
    	sum += p[i];
    }
    v2 = timing_stop();
    printf("read mmap'd ddr buffer:  %d cycles for size = 10000 words sum=%d\n", (v2-v1)/10000,sum);
    sleep(1); 
#endif

#if 0
//repeat with regular memory
 p = (long*) &buffer[0];
    v1 = timing_start();
    for(i=0;i<10000;i++)
    {
        sum += p[i];
    }
    v2 = timing_stop();
    printf("static ddr buffer:  %d cycles for size = 10000 words sum=%d\n", (v2-v1)/10000,sum);
    sleep(1);
#endif
  
#if 0
#ifdef AVOID_MMAP
p = (long*) virtp;
for(j=1;j<50;j++)
{
    v1 = timing_start();
    for(i=0;i<1000;i++)
    {
    if (hplibmod_cacheWb((void *)physp, 128*j)) return -1;
    }
    v2 = timing_stop();
    printf("wb (clean):  %d cycles for size = %d\n", (v2-v1)/1000, 128*j);
    sleep(1);
    sum=0;
    v1 = timing_start();
    for(i=0;i<1000;i++)
    {
    v3 = timing_stop();
    for(k=0;k<j*2;k++)  p[64*k] = i;
    v4 = timing_stop();
    sum += (v4-v3);
    if (hplibmod_cacheWb((void *)physp, 128*j)) return -1;
    }
    v2 = timing_stop();

    printf("wb (dirty):  %d cycles for size = %d\n", (v2-v1-sum)/1000, 128*j);
    sleep(1);
    v1 = timing_start();
    for(i=0;i<1000;i++)
    {
    if (hplibmod_cacheInv((void *)physp, 128*j)) return -1;
    }
    v2 = timing_stop();
    printf("Inv (no wb):  %d cycles for size = %d\n", (v2-v1)/1000, 128*j);
    sleep(1);
    v1 = timing_start();
    for(i=0;i<1000;i++)
    {
    if (hplibmod_cacheWbInv((void *)physp, 128*j)) return -1;
    }
    v2 = timing_stop();
    printf("wbInv (clean):  %d cycles for size = %d\n", (v2-v1)/1000, 128*j);
    sleep(1);
    sum=0;
    v1 = timing_start();
    for(i=0;i<1000;i++)
    {
    v3 = timing_stop();
    for(k=0;k<j*2;k++)  p[64*k] = i;
    v4 = timing_stop();
    sum += (v4-v3);
    if (hplibmod_cacheWbInv((void *)physp, 128*j)) return -1;
    }
    v2 = timing_stop();
    printf("wbInv (dirty):  %d cycles for size = %d\n", (v2-v1)/1000, 128*j);
}


//stress test
printf("stress test..\n");
{
volatile char *pp =  (char*) physp;
volatile char *vp =  (char*) virtp;
v1 = timing_start();
for(i=0,c=0;c<1000000000;c++)
{
    memcpy(vp,(char *)&i,4);
    memcpy(vp+64,(char *) &c,4);
    if (hplibmod_cacheWbInv((void *)pp, 128)) return -1;
    if (!(c%0xc0000)) {
         v2 = timing_stop();
          printf("%d ; ok %d cycles per wbinv call (128 bytes) %x %x\n",c, (v2-v1)/0xc0000, pp,vp); 
         v1 = timing_start();
    }
    if (i*128 > size-128){ i=0; pp = (char*) physp; vp = (char *) virtp;} 
    else {i+=128; pp+=128; vp+=128;}
}
}
#else
for(j=1;j<50;j++)
{
    v1 = timing_start();
    for(i=0;i<1000;i++)
    {
    if (hplibmod_cacheWb((void *)virtp, 128*j)) return -1;
    }
    v2 = timing_stop();
    printf("wb:  %d cycles for size = %d\n", (v2-v1)/1000, 128*j);
    sleep(1); 
    v1 = timing_start();
    for(i=0;i<1000;i++)
    {
    if (hplibmod_cacheInv((void *)virtp, 128*j)) return -1;
    }
    v2 = timing_stop();
    printf("Inv:  %d cycles for size = %d\n", (v2-v1)/1000, 128*j);
    sleep(1); 

    v1 = timing_start();
    for(i=0;i<1000;i++)
    {
    if (hplibmod_cacheWbInv((void *)virtp, 128*j)) return -1;
    }
    v2 = timing_stop();
    printf("wbInv:  %d cycles for size = %d\n", (v2-v1)/1000, 128*j);
}
#endif
#endif
    hplibmod_close();

    return 0;
}


