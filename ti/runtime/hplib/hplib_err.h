/*
 *   FILE NAME hplib_err.h
 *
 *      (C) Copyright 2012 Texas Instruments, Inc.
 * 
 *  Redistribution and use in source and binary forms, with or without 
 *  modification, are permitted provided that the following conditions 
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the   
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

 /**
 *   @file hplib_err.h
 *
 *   @brief
  *      Header file for the High Performance Library return values and error codes.
*/

#ifndef __HPLIB_ERR__H
#define __HPLIB_ERR__H

#ifdef __cplusplus
extern "C" {
#endif

/**
 *   @defgroup hplib_RetValue HPLIB API Return Values
 *   @ingroup @ingroup HPLIB_API
 */

typedef int hplib_RetValue;
/* @{ */
/**
 *  @def  hplib_OK
 *        HPLIB API return code -- API  executed successfully
 */
#define hplib_OK    0

/**
 *  @def  hplib_FAILURE
 *        HPLIB API return code -- API did NOT execute successfully
 */
#define hplib_FAILURE   1

/**
 *  @def  hplib_ERR_NOMEM
 *        HPLIB API return code -- API is returning out of memory error
 */
#define hplib_ERR_NOMEM   2

/**
 *  @def  hplib_ERR_BAD_INPUT
 *        HPLIB API return code -- API is returing argument or configuration is invalid
 */
#define hplib_ERR_BAD_INPUT   3


/**
 *  @def  hpLIB_ERR_QLLD
 *        HPLIB API return code -- API returing QUEUE Manager error
 */
#define hplib_ERR_QLLD  4

/**
 *  @def  hplib_ERR_NOTFOUND
 *        HPLIB API return code -- API returning the resource cannot be located
 */
#define hplib_ERR_NOTFOUND  5

/**
 *  @def  hplib_ERR_BUSY
 *        HPLIB API return code --API indicationg temoprarily out of resources or resource leak
 */
#define hplib_ERR_BUSY  6

/**
 *  @def  hplib_ERR_NORES
 *        HPLIB API return code --API returning no free hardware resources available
 */
#define hplib_ERR_NORES     9


/**
 *  @def  hplib_ERR_ALREADY_CANCELLED
 *        HPLIB API return code --API returning timere has already been cancelled
 */
#define hplib_ERR_ALREADY_CANCELLED     10

/*  @}  */  /* ingroup */

#ifdef __cplusplus
}
#endif

#endif
