/*
 *   FILE NAME hplib_util.h
 *
 *      (C) Copyright 2012 Texas Instruments, Inc.
 * 
 *  Redistribution and use in source and binary forms, with or without 
 *  modification, are permitted provided that the following conditions 
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the   
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

 /**
 *   @file hplib_util.h
 *
 *   @brief
  *      Header file for the High Performance Library utility API functions.
*/

#ifndef __HPLIB_UTIL__H
#define __HPLIB_UTIL__H

#ifdef __cplusplus
extern "C" {
#endif

#include <stdint.h>

#include <pthread.h>

#include <stdlib.h>
#include <sched.h>
#include <linux/unistd.h>
#include <sys/syscall.h>
#include <sys/sysinfo.h>
#include <ti/drv/cppi/cppi_drv.h>
#include <ti/drv/cppi/cppi_desc.h>
#include "hplib_err.h"
#include "hplib_sync.h"


/* Base address of timer64 */
#ifdef CORTEX_A8
extern volatile unsigned long * t64_virt_addr;
#endif


/**
 *  @def  HPLIB_MAX_MEM_POOLS
 *  @brief
 *      Define for MAX number of memory pools, currently set to 2 (DDR and MSMC)
 */
#define HPLIB_MAX_MEM_POOLS 2

/** @addtogroup HPLIB_UTIL_API
 @{ */

#ifdef CORTEX_A8
/* Return hw timer clock ticks per second */
unsigned long t64_ticks_sec(void);

/**
 *  @b Description
 *  @n  
 *      The function API returns hardware timer clock ticks per second.
 *
 *  @retval    unsigned long clock ticks per second
 	
 */
#define hplib_mUtilGetTicksPerSec t64_ticks_sec

/**
 *  @b Description
 *  @n
 *      The function API returns a 64bit H/W timestamp by reading a H/W timer 
 *      and returns a 64 bit time stamp. The frequency can be determined by 
 *      calling @ref hplib_getTicksPerSec
 *
 *  @retval
        unsigned long long timestamp (64 bits)
 */

static inline unsigned long long hplib_mUtilGetTimestamp(void)
{
    volatile unsigned long long t1;
    volatile unsigned long long t2;
    unsigned long long val;
    t1 = t64_virt_addr[0x10/4]; /* low */
    t2 = t64_virt_addr[0x14/4]; /* hi */
    val = ((unsigned long long) t2) <<32 | t1;
    return val;
}
#else
/**
 *  @b Description
 *  @n  
 *      The function API returns hardware timer clock ticks per second
 *
 *  @retval    unsigned long clock ticks per second
 	
 */
static inline long hplib_mUtilGetTicksPerSec(void)
{
    long val;
    asm volatile("mrc p15, 0, %0, c14, c0, 0" :  "=r"(val));
    return val;
}

/**
 *  @b Description
 *  @n
 *      The function API returns a 64bit H/W timestamp by reading a H/W timer
 *      The frequency can be determined by 
 *      calling @ref hplib_mUtilGetTicksPerSec.
 *
 *  @retval
        unsigned long long timestamp (64 bits)
 */
static inline unsigned long long hplib_mUtilGetTimestamp(void)
{
    unsigned long vall;
    unsigned long valh;
    unsigned long long val;
    asm volatile("mrrc p15, 0, %0, %1, c14" :  "=r"(vall),"=r"(valh));
    val = ((unsigned long long) valh)<<32 | vall;
    return val;
}

#endif


/**
 *  @b Description
 *  @n  
 *      The function API is used to read the current ARM PMU CCNT register
  *     (counting cpu cycles)
 *  *  @retval  long
 *         Current cpu cycle count from CCNT register
 */
static inline unsigned long hplib_mUtilGetPmuCCNT(void)
{
    volatile int vval;
    /*read PMU ccnt register */
    asm volatile("mrc p15, 0, %0, c9, c13, 0" :  "=r"(vval));
    return vval;
}

/**
 *  @b Description
 *  @n  
 *     read PMCx register, counter slot needs to be programmed already
 *  *  @retval  long
 *         counter value
 */
static inline long hplib_mUtilReadPmuCounter(void)
{
    long val;
    asm volatile("mrc p15, 0, %0, c9, c13, 2" :  "=r"(val));
    return val;
}

/**
 *  @b Description
 *  @n  
 *      Enable all four PMU event counters (slots)
 */
static inline void hplib_mUtilWritePmuEnableAllCounters(void)
{
    long val;
    asm volatile("mrc p15, 0, %0, c9, c12, 1" :  "=r"(val));
    val |=  0xf;
    asm volatile("mcr p15, 0, %0, c9, c12, 1" : : "r"(val));
}


/**
 *  @b Description
 *  @n  
 *      write PMNXSEL register, this tells which counter slot we are currently working with
 */
static inline void hplib_mUtilWritePmuSelectCntr(long val)
{
    asm volatile("mcr p15, 0, %0, c9, c12, 5" : : "r"(val));
}

/**
 *  @b Description
 *  @n  
 *      write EVTSEL register, indicate which event should be counted by the
 *      current counter slot.
 */
static inline void hplib_mUtilWritePmuEventToCount(long val)
{
    asm volatile("mcr p15, 0, %0, c9, c13, 1" : : "r"(val));
}


/**
 *  @b Description
 *  @n  
 *      Program counter slot: slot to count event: event
 */
static inline void hplib_mUtilProgramPmuEvent(int slot, int event)
{
    hplib_mUtilWritePmuSelectCntr(slot);
    hplib_mUtilWritePmuEventToCount(event);
}


/**
 *  @b Description
 *  @n  
 *      Read event counter in slot: slot
 */
static inline long hplib_mUtilReadPmuEvent(int slot)
{
    hplib_mUtilWritePmuSelectCntr(slot);
    return hplib_mUtilReadPmuCounter();
}

/*
 *  @b Description
 *  @n
*      The function API is used to schedule the calling thread to run on the
*      specified cpu set, as determined by ref @ pSet, enables user space
*      access to arm system timer and the core performance monitor unit (PMU)
*      for specified @ref coreId.

 *  @param[in] threadId
 *      Thread Id of calling thread
 *  @param[in] pSet
 *      CPU set contains set of CPU's calling thread can be scheduled on to run.
 *  @param[in] if_type
 *      Specifies which set of HPLIB spinlock APIs to use.
 *      2 set of interface APIs are provided, one for Low overhead Linux use case
 *      and one for Multi Process use case. @ref hplib_spinLock_Type
 *  *  @retval  @ref hplib_RetValue
 */
hplib_RetValue hplib_utilSetupThread(int threadId,
                                     cpu_set_t *pSet,
                                     hplib_spinLock_Type if_type);

/**
@}
*/
/*
 *  @b Description
 *  @n
*      The function API is called 1 time per system boot by the global
*      master process to specify the set of HPLIB spinlock APIs to use.

 *  @param[in] type
 *      Specifies which set of HPLIB spinlock APIs to use.
 *      2 set of interface APIs are provided, one for Low overhead Linux use case
 *      and one for Multi Process use case.
 *  *  @retval  @ref hplib_RetValue
 */
/* This is called 1 Time per system boot by the global master process */
hplib_RetValue hplib_utilOsalCreate(void);

#ifdef __cplusplus
}
#endif

#endif
