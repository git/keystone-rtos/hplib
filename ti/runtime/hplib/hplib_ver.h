/*******************************
 * file: hplib_ver.h
 * purpose: hplib version information
 **************************************************************
 * @file hplib_ver.h
 * 
 * @brief DESCRIPTION: hplib version information for user space transport
 *               library
 * 
 * REVISION HISTORY:
 *
 *  Copyright (c) Texas Instruments Incorporated 2013
 * 
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the   
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 ******************************/

#ifndef __HPLIB_VER__
#define __HPLIB_VER__

#ifdef __cplusplus
extern "C" {
#endif


/**
 * @def HPLIB_VERSION_ID
 *      This is the HPLIB Version. Versions numbers are encoded in the following 
 *      format:
 *              0xAABBCCDD -> Arch (AA); API Changes (BB); Major (CC); Minor (DD)
 */
#define HPLIB_VERSION_ID                   (0x01010008)

/**
 * @def HPLIB_VERSION_STR
 *      This is the version string which describes the HPLIB along with the
 *      date and build information.
 */
#define HPLIB_VERSION_STR      "HPLIB Revision: 01.01.00.08"

/**
 *  @ingroup hplib_gen_functions
 *  @brief hplib_mGetVersion  API used to get version of HPLIB 
 *
 *  @details This  API used to get version of hplib
 *  @retval     int version of hplib
 */
static inline int hplib_getVersion(void)
{
return HPLIB_VERSION_ID;
}

/**
 *  @ingroup hplib_gen_functions
 *  @brief hplib_mGetVersionString  API used to get version string of HPLIB 
 *
 *  @details This  API used to get version string of hplib
 *  @retval     char version string of hplib
 */
static inline char * hplib_getVersionString(void) { return HPLIB_VERSION_STR;}

#ifdef __cplusplus
}
#endif


#endif
