/*
 * File name: hplibmod.c
 *
 * Description: HPLIB utility module.
 *
 * Copyright (C) 2013 Texas Instruments, Incorporated
 * 
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation version 2.
 *
 * This program is distributed "as is" WITHOUT ANY WARRANTY of any kind,
 * whether express or implied; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 */


#include <linux/init.h>
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/fs.h>
#include <linux/sysfs.h>
#include <linux/version.h>
#include <linux/unistd.h>
#include <asm/unistd.h>
#include <linux/wait.h>
#include <linux/sched.h>
#include <linux/timer.h>
#include <linux/string.h>
#include <linux/if.h>

#include <asm/uaccess.h>
#include <linux/dma-mapping.h>

#include <asm/irq.h>
#include <linux/clk.h>

#include <asm/mach/map.h>

#include <linux/hrtimer.h>

#include "../hplibmod.h"

#include <asm/pgtable.h>

#define HPLIBMOD_DEBUG

#define HPLIBMOD_DEVNAME  "hplib"

#ifdef HPLIBMOD_DEBUG
#  define __D(fmt, args...) printk(KERN_DEBUG "HPLIBMOD Debug: " fmt, ## args)
#else
#  define __D(fmt, args...)
#endif

#define __E(fmt, args...) printk(KERN_ERR "HPLIBMOD Error: " fmt, ## args)

#define LINE_LEN 50
static struct class *hplibmod_class;
static int hplibmod_major;

static dma_addr_t dmaAddr; /*physical address of CMA region */
static void * cpuAddr=NULL; /* virtual address for above */
static void * userVmaStart = NULL;

static unsigned int memSize = HPLIBMOD_MEMSZ;
module_param(memSize, uint, 0);
MODULE_PARM_DESC(memSize, "Size of DMA coherent memory to be allocated");

#ifdef CORTEX_A8
static void MPU_Enable_userModeAccess(void)
{
    unsigned int reg_addr,i,count,defRegVal;
    void __iomem * temp_reg;
    defRegVal = 0x3FFFFFF;
    reg_addr = 0x2368208;
    count = 5;

    for(i=1;i<=count;i++) {
    temp_reg= ioremap(reg_addr, 4);
    __raw_writel(defRegVal, temp_reg) ;
    iounmap(temp_reg);
    reg_addr = reg_addr + 0x10;
    }

    reg_addr = 0x2370208;
    count = 16;

    for(i=1;i<=count;i++) {
    temp_reg= ioremap(reg_addr, 4);
    __raw_writel(defRegVal,temp_reg) ;
    iounmap(temp_reg);
    reg_addr = reg_addr + 0x10;
    }

    reg_addr = 0x2378208;
    count = 1;

    for(i=1;i<=count;i++) {
     temp_reg= ioremap(reg_addr, 4);
    __raw_writel(defRegVal, temp_reg) ;
    iounmap(temp_reg);

    reg_addr = reg_addr + 0x10;
    }
}
#endif

static ssize_t hplib_show(struct device *dev,
                          struct device_attribute *attr,
                          char *buf)
{
    int len = 0;

    len += snprintf(buf + len, LINE_LEN, "HPLIBMOD Info\n");
    len += snprintf(buf + len, LINE_LEN, "======================\n");
    len += snprintf(buf + len, LINE_LEN, "CMA memory properties :\n");
    len += snprintf(buf + len, LINE_LEN, "Device viewed CMA physical address 0x%x\n", (unsigned int) dmaAddr);
    len += snprintf(buf + len, LINE_LEN, "CPU    viewed CMA virtual  address 0x%x\n", (unsigned int) cpuAddr);
    len += snprintf(buf + len, LINE_LEN, "Size   of CMA allocated            0x%x\n", memSize);

    return len;

}

static DEVICE_ATTR(hplib, S_IRUGO, hplib_show, NULL);

static int hplibmod_core_setup(void)
{
   unsigned long  val;


   //enable user access to qmss h/w
#ifdef CORTEX_A8
  MPU_Enable_userModeAccess();
#endif
   //pmcr <- single clock, reset, enable
   val = 0x4|0x1;
   asm volatile("mcr p15, 0, %0, c9, c12, 0" : : "r"(val));

    /*userenr <- enable user space access */
   val = 1;
   asm volatile("mcr p15, 0, %0, c9, c14, 0" : : "r"(val));

   val = 0x80000000;
   asm volatile("mcr p15, 0, %0, c9, c12, 1" ::  "r"(val));

    asm volatile("mrc p15, 0, %0, c9, c13, 0" :  "=r"(val));
#ifndef CORTEX_A8
    /* enabling of arm timer for user space */
   asm volatile("mrc p15, 0, %0, c14, c1, 0" :  "=r"(val));
    val |=  0x3; 
    asm volatile("mcr p15, 0, %0, c14, c1, 0" : : "r"(val));
#endif

    return  0;
}
static long hplibmod_ioctl(struct file *filp, unsigned int cmd, unsigned long args)
{
    unsigned int __user *argp = (unsigned int __user *) args;
    struct hplibmod_block block;
    unsigned long physp;

    if (_IOC_TYPE(cmd) != _IOC_TYPE(HPLIBMOD_IOCMAGIC)) {
        __E("ioctl(): bad command type 0x%x (should be 0x%x)\n",
            _IOC_TYPE(cmd), _IOC_TYPE(HPLIBMOD_IOCMAGIC));
    }

    switch (cmd & HPLIBMOD_IOCCMDMASK) {
        case HPLIBMOD_IOINIT:
            __D("INIT ioctl received.\n");
            hplibmod_core_setup();
            break;

        case HPLIBMOD_IOCGETPHYS:
            __D("GETPHYS ioctl received.\n");

            if (put_user((unsigned long)dmaAddr, argp)) {
                return -EFAULT;
            }

            __D("GETPHYS returning %#llx\n", (long long)dmaAddr);
            break;

        case HPLIBMOD_IOCCACHE:
            __D("CACHE%s%s ioctl received.\n",
                cmd & HPLIBMOD_WB ? "WB" : "", cmd & HPLIBMOD_INV ? "INV" : "");

            if (copy_from_user(&block, argp, sizeof(block))) {
                return -EFAULT;
            }
            physp = (unsigned long)dmaAddr +
                    (block.addr - (unsigned long)userVmaStart);

            switch (cmd & ~HPLIBMOD_IOCMAGIC) {
                case HPLIBMOD_IOCCACHEWB:
                    dma_sync_single_for_device(NULL, (dma_addr_t)physp,
                                               block.size, DMA_TO_DEVICE);
                    break;

                case HPLIBMOD_IOCCACHEINV:
                    dma_sync_single_for_cpu(NULL, (dma_addr_t)physp,
                                            block.size, DMA_FROM_DEVICE);
                    break;

                case HPLIBMOD_IOCCACHEWBINV:
                    dma_sync_single_for_device(NULL, (dma_addr_t)physp,
                                               block.size, DMA_TO_DEVICE);
                    dma_sync_single_for_cpu(NULL, (dma_addr_t)physp,
                                            block.size, DMA_FROM_DEVICE);
                    break;

            } /* switch cmd */ 

            __D("CACHE%s%s ioctl returned vaddr=0x%p size=0x%x paddr=0x%p.\n",
                cmd & HPLIBMOD_WB ? "WB" : "",
                cmd & HPLIBMOD_INV ? "INV" : "",
                (void *)block.addr, block.size, (void *)physp);
            break;

        case HPLIBMOD_IOCGETSIZE:
            __D("GETSIZE ioctl received.\n");

            if (put_user((unsigned long)memSize, argp)) {
                return -EFAULT;
            }

            __D("GETSIZE returning %#1x\n", (unsigned int)memSize);
            break;

        default:
            __E("Unknown ioctl received.\n");
            return -EINVAL;

    } /* switch base cmd */

    return 0;
}

#define PROT_PTE_DEVICE	(L_PTE_PRESENT|L_PTE_YOUNG|L_PTE_DIRTY|L_PTE_XN)

#define hplibmod_pgprot_dev_buffer(prot) \
    __pgprot_modify(prot, L_PTE_MT_MASK, PROT_PTE_DEVICE | \
    L_PTE_MT_DEV_SHARED | L_PTE_SHARED)

static int hplibmod_mmap(struct file *filp, struct vm_area_struct *vma)
{
    size_t size = vma->vm_end - vma->vm_start;

    __D("mmap: vma->vm_start     = %#lx\n", vma->vm_start);
    __D("mmap: vma->vm_pgoff     = %#lx\n", vma->vm_pgoff);
    __D("mmap: vma->vm_end       = %#lx\n", vma->vm_end);
    __D("mmap: size              = 0x%x\n", size);

    switch ((vma->vm_pgoff << PAGE_SHIFT))
    {
        case HPLIBMOD_MMAP_DMA_MEM_OFFSET:
            userVmaStart = (void *)vma->vm_start;
            __D("mmap: calling dma_mmap_coherent\n");
            return dma_mmap_coherent(NULL, vma, cpuAddr, dmaAddr, size);
            break;

        case HPLIBMOD_MMAP_QM_DATA_REG_MEM_OFFSET:
            vma->vm_page_prot = hplibmod_pgprot_dev_buffer(vma->vm_page_prot);
            if (remap_pfn_range(vma, vma->vm_start, vma->vm_pgoff, size,
                                vma->vm_page_prot)) {
                return -EAGAIN; 
            }
            __D("mmap: page_prot=%#lx\n", (long unsigned int) vma->vm_page_prot);
            return 0;
            break;

        default:
            return -EINVAL;
    }
}

static int hplibmod_open(struct inode *inode, struct file *filp)
{
    __D("open: called.\n");
    return 0;
}

static int hplibmod_release(struct inode *inode, struct file *filp)
{
    __D("close: called.\n");
    return 0;
}

static struct file_operations hplibmod_fxns = {
    owner:              THIS_MODULE,
    unlocked_ioctl:     hplibmod_ioctl,
    mmap:               hplibmod_mmap,
    open:               hplibmod_open,
    release:            hplibmod_release
};
static struct device *hplib_dev;

/*********************************************************************************
* FUNCTION: hplibmod_init_module
*
* DESCRIPTION:Initialization routine for hplib kernel device
*********************************************************************************/
static int __init hplibmod_init_module(void)
{
    int ret;
    
     __D("init\n");

    hplibmod_major = register_chrdev(0, HPLIBMOD_DEVNAME, &hplibmod_fxns);

    if (hplibmod_major < 0) {
        __E("Failed to allocate major number.\n");
        return -ENODEV;
    }

    __D("Allocated major number: %d\n", hplibmod_major);

    hplibmod_class = class_create(THIS_MODULE, HPLIBMOD_DEVNAME);
    if (IS_ERR(hplibmod_class)) {
        __E("Error creating hplib device class.\n");
        ret = PTR_ERR(hplibmod_class);
        goto cleanup_dev;
    }

    hplib_dev = device_create(hplibmod_class, NULL, MKDEV(hplibmod_major, 0),
			      NULL, HPLIBMOD_DEVNAME);
    hplib_dev->coherent_dma_mask = DMA_BIT_MASK(32);
    hplib_dev->dma_pfn_offset = 0x780000UL;

    if (IS_ERR(hplib_dev)) {
        __E("Error creating hplib device .\n");
        ret = PTR_ERR(hplib_dev);
        goto cleanup_dev;
    }
    ret = device_create_file(hplib_dev,&dev_attr_hplib);
    WARN_ON(ret < 0);

    /* allocate space from CMA */
    cpuAddr = dma_alloc_coherent(hplib_dev, memSize, &dmaAddr,
				 GFP_KERNEL | GFP_DMA);

    if (!cpuAddr) {
        __E("Error allocating from CMA for size 0x%x\n", memSize);
        ret = -ENOMEM;
	goto cleanup_dev;
    } else {
        __D("hplibmod_init_module: Allocated 0x%x size memory from CMA.\n",memSize);
        __D("hplibmod_init_module: dmaAddr %#llx\n", (long long)dmaAddr);
    }

    __D("module loaded\n");

    return 0;

cleanup_dev:
    unregister_chrdev(hplibmod_major, HPLIBMOD_DEVNAME);

    return ret;
}

/*********************************************************************************
 * FUNCTION: hplibmod_cleanup_module
 *
 * DESCRIPTION:
 *********************************************************************************/
static void __exit hplibmod_cleanup_module(void)
{
    /* Free CMA buffer */
     dma_free_coherent(hplib_dev, memSize, cpuAddr, dmaAddr);

    /* Remove netapi device */
    device_destroy(hplibmod_class, MKDEV(hplibmod_major, 0));
    unregister_chrdev(hplibmod_major, HPLIBMOD_DEVNAME);
    class_destroy(hplibmod_class);

    __D("module unloaded\n");
    return;
}

module_init(hplibmod_init_module);
module_exit(hplibmod_cleanup_module);


MODULE_AUTHOR("Texas Instruments Incorporated");
MODULE_DESCRIPTION("TI HPLIB kernel module.");
MODULE_SUPPORTED_DEVICE("Texas Instruments hplib");
MODULE_LICENSE("GPL v2");
