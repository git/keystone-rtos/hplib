diff --git arch/arm/include/asm/pgtable.h arch/arm/include/asm/pgtable.h
index f66626d..95bd431 100644
--- arch/arm/include/asm/pgtable.h
+++ arch/arm/include/asm/pgtable.h
@@ -105,7 +105,7 @@ extern pgprot_t		pgprot_kernel;
 
 #ifdef CONFIG_ARM_DMA_MEM_BUFFERABLE
 #define pgprot_dmacoherent(prot) \
-	__pgprot_modify(prot, L_PTE_MT_MASK, L_PTE_MT_BUFFERABLE | L_PTE_XN)
+	__pgprot_modify(prot, L_PTE_MT_MASK, L_PTE_MT_WRITEBACK| L_PTE_XN)
 #define __HAVE_PHYS_MEM_ACCESS_PROT
 struct file;
 extern pgprot_t phys_mem_access_prot(struct file *file, unsigned long pfn,
