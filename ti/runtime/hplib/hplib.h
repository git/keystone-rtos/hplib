/**
 *   @file hplib.h
 *
 *   @brief
 *      Header file for the High Performance Library. The file includes the HPLIB header files
 *      which include the data structures and exported API which are available 
 *      for use by applications.
 *
 *  \par
 *  NOTE:
 *      (C) Copyright 2012 Texas Instruments, Inc.
 * 
 *  Redistribution and use in source and binary forms, with or without 
 *  modification, are permitted provided that the following conditions 
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the   
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */


/**  @mainpage High Performance Library Application Program Interface
 *
 *   @section intro  Introduction
 *
 * The High Performance library will provide efficient implementation of ARM Linux user space utilities needed by user space applications of which network transport is the first focus. 
 * The library will consist of the following:
 * \par
 *      (1) Set of API's grouped by functional area such as synchronization API's, Virtual Memory API's, 
 *           basic utility API's and Timer API's.
 * \par
 *      (2) Kernel module which will be use Continuous Memory Allocation (CMA) to allocate a chunk of 
 *           cached physical memory from the kernel to be used by user space applications.  
*/


#ifndef __HP_LIB_H__
#define __HP_LIB_H__

#ifdef __cplusplus
extern "C" {
#endif


#include "hplib_err.h"
#include "hplib_util.h"
#include "hplib_vm.h"
#include "hplib_sync.h"
#include "hplib_cache.h"
#include "hplib_shm.h"

/* Define HPLIB_API as a master group in Doxygen format and 
 * add all HPLIB API definitions to this group.
 */
/** @defgroup HPLIB_API
 *  @{
 */
/** @} */

/**
@defgroup HPLIB_SYNC_API  High Performance Library Sync API's
@ingroup HPLIB_API
*/
/**
@defgroup HPLIB_UTIL_API  High Performance Library Utility API's
@ingroup HPLIB_API
*/

/**
@defgroup HPLIB_VM_API High Performance Library Virtual Memory Allocator API's
@ingroup HPLIB_API
*/

#ifdef __cplusplus
}
#endif

#endif

